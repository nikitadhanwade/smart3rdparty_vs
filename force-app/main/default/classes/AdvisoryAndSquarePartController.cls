public class AdvisoryAndSquarePartController {
    
    public string currentRecordId{get;set;}
    public string advisory{get;set;}
    public string sparePart{get;set;}
    public string advisoryText{get;set;}
    public string sparePartText{get;set;}
    public boolean displayPopup {get; set;} 
    public string advisoryHelpText{get;set;}
    public string sparePartHelpText{get;set;}
    public string advisoryLabel{get;set;}
    public string sparePartLabel{get;set;}
    public AdvisoryAndSquarePartController(ApexPages.StandardController controller){
        
        currentRecordId=controller.getId();
        System.debug('currentRecordId1-----------'+currentRecordId);
        Case caseRecord=getCaseRecord(currentRecordId);
        
        if(caseRecord != null){
            advisory=caseRecord.account.Advisory__c;
            sparePart=caseRecord.account.Spare_Part__c;
            advisoryHelpText=Account.Advisory__c.getDescribe().getInlineHelpText();
            sparePartHelpText=Account.Spare_Part__c.getDescribe().getInlineHelpText();
            advisoryLabel=Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().get('Advisory__c').getDescribe().getLabel();
            sparePartLabel=Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().get('Spare_Part__c').getDescribe().getLabel();
            displayPopup = false;
        }
        System.debug('advisory-----------'+advisory);
        System.debug('sparePart-----------'+sparePart);
    }
    public PageReference advisoryMethod(){
        Id id1;
        if(test.isRunningTest()){
            id1 =currentRecordId;
        }else{
            id1 = System.currentPageReference().getParameters().get('keyId');
        }
        Case caseRecord=getCaseRecord(id1);
        
        if(caseRecord != null){
            advisoryText=caseRecord.account.Advisory__c;
            displayPopup = true;
        }
        System.debug('advisoryText-----------'+advisoryText);
        System.debug('id1-----------'+id1);
        return null;
    }
    public PageReference squarePartMethod(){
        Id id1;
        if(test.isRunningTest()){
            id1 =currentRecordId;
        }else{
            id1 = System.currentPageReference().getParameters().get('keyId1');
        }
        Case caseRecord=getCaseRecord(id1);
        
        if(caseRecord != null){
           sparePartText=caseRecord.account.Spare_Part__c;
            displayPopup = true;
        }
        System.debug('id1-----------'+id1);
        return null;
    }
    
    public Case getCaseRecord(String recordId){
       return [select Status,account.Advisory__c,account.Spare_Part__c from case  where id =: recordId];
    }
    public void closePopup()
    {       
        displayPopup = false;   
    } 
}