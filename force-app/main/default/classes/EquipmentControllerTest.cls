@isTest
public class EquipmentControllerTest {
    @isTest
    public static void testCases() {
        
        
        Id p = [select id from profile where name='S3P Customer Community Login User'].id;
        
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = accObj.id;
        insert conObj;
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = conObj.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        
        
        
        Site__c siteObj = new SIte__c();
        siteObj.Name = 'test Site';
        siteObj.Location__Latitude__s = 37.791882;
        siteObj.Location__Longitude__s =  -122.394339;
        siteObj.Customer_Account__c = accObj.id;
        insert siteObj; 
        List<Equipment__c> equipmentList = new List<Equipment__c>();
        for(integer i =0;i<6;i++) {
            Equipment__c equipmentObj = new Equipment__c();
            equipmentObj.Account__c =  accObj.id;
            equipmentObj.Site__c  = siteObj.id;
            equipmentList.add(equipmentObj);
        }
        insert equipmentList;
        system.runAs(user) {
            EquipmentController.findAll();
        }
        System.assertEquals(equipmentList != null, true);
        
    }
}