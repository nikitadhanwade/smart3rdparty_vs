@isTest
public class UploadCSVFileOfAssetTest {

    
    @istest static void insertRecordDoc(){  
        UploadCSVFileOfAsset.fetchDocument();
     }
    @istest static void uploadCSVFileTest(){
       
        String csvFileBody = 'Manufacturer,Model Name,Model Number,Serial Number,Support Level,Start Date,End Date,Equipment Covered\r\n Dell,Dell,123,KLJ123,Lghgh,7/23/2019,7/31/19,Test';
        Blob csvBlobData = Blob.valueOf(csvFileBody);
      
        UploadCSVFileOfAsset.readFile(csvBlobData);
        }
    @istest static void monthGreaterTest(){
         Id p = [select id from profile where name='S3P Customer Community Login User'].id;
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = accObj.id;
        insert conObj;
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = conObj.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
       
        Test.startTest();
        system.runAs(user) {
        String csvFileBody = 'Manufacturer,Model Name,Model Number,Serial Number,Support Level,Start Date,End Date,Equipment Covered\r\n Dell,Dell,123,KLJ123,L1,13/23/2019,7/31/19,Test';
        Blob csvBlobData = Blob.valueOf(csvFileBody);
        UploadCSVFileOfAsset ucfa = new UploadCSVFileOfAsset();
        
           UploadCSVFileOfAsset.readFile(csvBlobData);
        }
        Test.stopTest();
    }
    @istest static void daysInvalidTest(){
         Id p = [select id from profile where name='S3P Customer Community Login User'].id;
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = accObj.id;
        insert conObj;
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = conObj.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        
        Test.startTest();
         system.runAs(user) {
        String csvFileBody = 'Manufacturer,Model Name,Model Number,Serial Number,Support Level,Start Date,End Date,Equipment Covered\r\n Dell,Dell,123,KLJ123,L1,04/31/2019,7/31/19,Test';
        Blob csvBlobData = Blob.valueOf(csvFileBody);
        UploadCSVFileOfAsset ucfa = new UploadCSVFileOfAsset();
       
           UploadCSVFileOfAsset.readFile(csvBlobData);
        }
        Test.stopTest();
    }
    @istest static void duplicateRecordTest(){
          Id p = [select id from profile where name='S3P Customer Community Login User'].id;
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        Equipment__c equip = new Equipment__c();
        equip.Account__c=accObj.id;
        equip.Manufacturer__c='Dell';
        equip.Non_Smart3rdParty_Asset__c=true;
        
        equip.Model_Name__c='Dell';
        equip.Model_Number__c='123';
        equip.Serial_Number__c='KLJ123';
        equip.Support_Level__c='Lghgh';
        equip.Start_Date__c=Date.valueOf('2019-07-23 00:00:00');
        equip.End_Date__c=Date.valueOf('2019-07-31 00:00:00');
        equip.Equipment_Covered__c='Test';
        insert equip;
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = accObj.id;
        insert conObj;
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = conObj.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        
        Test.startTest();
         system.runAs(user) {
        String csvFileBody = 'Manufacturer,Model Name,Model Number,Serial Number,Support Level,Start Date,End Date,Equipment Covered\r\n Dell,Dell,123,KLJ123,L1,6/15/2019,7/16/19,Test\r\n Dell1,Dell1,1231,KLJ123,L11,3/20/2019,4/21/19,Test1';
        Blob csvBlobData = Blob.valueOf(csvFileBody);
        UploadCSVFileOfAsset ucfa = new UploadCSVFileOfAsset();
        
           UploadCSVFileOfAsset.readFile(csvBlobData);
        }
        Test.stopTest();
    }
    @istest static void startDateGreaterThanEndDateTest(){
         Id p = [select id from profile where name='S3P Customer Community Login User'].id;
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = accObj.id;
        insert conObj;
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = conObj.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
       
        Test.startTest();
         system.runAs(user) {
        String csvFileBody = 'Manufacturer,Model Name,Model Number,Serial Number,Support Level,Start Date,End Date,Equipment Covered\r\n Dell,Dell,123,KLJ123,L1,8/30/2019,7/31/19,Test';
        Blob csvBlobData = Blob.valueOf(csvFileBody);
        UploadCSVFileOfAsset ucfa = new UploadCSVFileOfAsset();
      
           UploadCSVFileOfAsset.readFile(csvBlobData);
        }
        Test.stopTest();
    }
    @istest static void getResellerNameTest(){
         Id p = [select id from profile where name='S3P Customer Community Login User'].id;
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = accObj.id;
        insert conObj;
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = conObj.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
       
        Test.startTest();
        system.runAs(user) {
        
        UploadCSVFileOfAsset ucfa = new UploadCSVFileOfAsset();
        
           UploadCSVFileOfAsset.getResellerName();
        }
        Test.stopTest();
    }
}
         /*Id p = [select id from profile where name='S3P Customer Community Login User'].id;
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        
        
        
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = accObj.id;
        insert conObj;
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = conObj.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
       
        Test.startTest();
        system.runAs(user) {
        String csvFileBody = 'Manufacturer,Model Name,Model Number,Serial Number,Support Level,Start Date,End Date,Equipment Covered\r\n Dell,Dell,123,KLJ123,Lghgh,7/23/2019,7/31/19,Test';
        Blob csvBlobData = Blob.valueOf(csvFileBody);
        UploadCSVFileOfAsset ucfa = new UploadCSVFileOfAsset();
         
           UploadCSVFileOfAsset.readFile(csvBlobData);
        }
        
        Test.stopTest();*/
    
    /*
    
     
    */