public class GraphController {
    
    
    @auraEnabled
    public static Map<string, integer> getChartMap(){
        List<Case> caseStatusList = new List<Case>();
        List<User> userObj = [SELECT id,ContactId FROM USER Where Id =: UserInfo.getUserId()];
		
		if(userObj != null && userObj[0] != null) {
			List<Contact> contactList = [SELECT id,Name,AccountId 
                                     	   FROM Contact 
                                          WHERE Id = : userObj[0].ContactId];  
            if(contactList != null && contactList.size() > 0 && contactList[0] != null) {
                caseStatusList = [SELECT Status FROM Case Where Account.id = :contactList[0].AccountId];
            }else {
               caseStatusList = [SELECT Status FROM Case];  
            }
        }else {
           caseStatusList = [SELECT Status FROM Case]; 
        }
        Schema.DescribeFieldResult F = Case.status.getDescribe();
        Map<string, integer> mp = new Map<string, integer>();  
		List<Schema.PicklistEntry> pickListValues = F.getPicklistValues();
        for(Schema.PicklistEntry  value:pickListValues) {
            mp.put(String.ValueOf(value.getValue()),0);
        }
        
        for(Case caseObj :caseStatusList) {
            if(mp.containsKey(caseObj.Status)) {
                integer count = mp.get(caseObj.Status);
                mp.put(caseObj.Status,count+1);
            }else {
                mp.put(caseObj.Status,1);
            }
            
        }
        System.debug(mp);
        return mp;
    }
    
    /*@auraEnabled
    public static string getLineChartMap(){
        List<LineChartVar> myLineChartVarList = new List<LineChartVar>();
        
        myLineChartVarList.add(new LineChartVar('2010', 10, 12));
        myLineChartVarList.add(new LineChartVar('2011', 12, 15));
        myLineChartVarList.add(new LineChartVar('2012', 17, 19));
        myLineChartVarList.add(new LineChartVar('2013', 19, 17));
        myLineChartVarList.add(new LineChartVar('2014', 21, 15));
        myLineChartVarList.add(new LineChartVar('2015', 17, 20));
        myLineChartVarList.add(new LineChartVar('2016', 18, 16));
        
        System.debug('asdf:    '+myLineChartVarList);
        
        return JSON.Serialize(myLineChartVarList);
    }*/
}