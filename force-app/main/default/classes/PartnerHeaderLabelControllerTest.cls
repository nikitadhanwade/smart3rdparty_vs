@istest
public class PartnerHeaderLabelControllerTest {

    @istest public static void getAccountName(){
        Id p = [select id from profile where name='S3P Partner Community User'].id;
        
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = accObj.id;
        insert conObj;
        
        User user = new User(alias = 'test1234', email='test1234@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = conObj.Id,
                             timezonesidkey='America/Los_Angeles', username='tester4@noemail.com');
        
        insert user;
        system.runAs(user) {
           PartnerCommunityHeaderLabelController.getAccountName();
        }
    }
}