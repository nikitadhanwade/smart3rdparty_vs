public class CreateCase {

    public static Integer pageSize{get;set;}
    public static Integer offset{get;set;}
    
    @AuraEnabled
    public static List<Equipment__c> getAllSerialNumber() {
        
        list<Equipment__c> equipList = [SELECT Id,Account__r.name,Support_Level__c,Equipment_Covered__c,Model_Name__c,Model_Number__c,Start_Date__c ,End_Date__c,Name,Purchase_Date__c,Serial_Number__c,Site__r.name,Manufacturer__c FROM Equipment__c where Serial_Number__c!=null ORDER BY createdDate desc];
        return equipList;
        
    }
     @AuraEnabled
    public static List<Equipment__c> findSerialNumber(String searchKey) {
        List<Equipment__c> equip = new List<Equipment__c>();
        List<Contact> contactList = new List<Contact>();
        if(!String.isBlank(searchKey) || searchKey != ''){
            String SerialNumber = searchKey + '%';
            System.debug('SerialNumber----------'+SerialNumber);
            List<User> userObj = [SELECT id,ContactId FROM USER Where Id =: UserInfo.getUserId()];
           
            if(userObj != null && userObj[0] != null) {
                contactList = [SELECT id,Name,AccountId 
                                             FROM Contact 
                                             WHERE Id = : userObj[0].ContactId];
            
                if(contactList != null && contactList.size() > 0 && contactList[0] != null) {
                    equip =[ SELECT Id,Account__r.name,Equipment_Covered__c,Support_Level__c,Model_Name__c,Model_Number__c,Start_Date__c ,End_Date__c,Name,Purchase_Date__c,Serial_Number__c,Site__r.name,Manufacturer__c FROM Equipment__c WHERE (Account__c = :contactList[0].AccountId) AND (Serial_Number__c LIKE :SerialNumber OR Manufacturer__c LIKE :SerialNumber OR Account__r.name LIKE :SerialNumber OR Model_Name__c LIKE :SerialNumber OR Model_Number__c LIKE :SerialNumber OR Site__r.name LIKE :SerialNumber OR Support_Level__c LIKE :SerialNumber)  ORDER BY createdDate desc];
                }
                System.debug('equip----------'+equip);
            }
            //equip =[ SELECT Id,Account__r.name,Start_Date__c,Support_Level__c ,End_Date__c,Equipment_Covered__c,Model_Name__c,Model_Number__c,Name,Purchase_Date__c,Serial_Number__c,Site__r.name,Manufacturer__c FROM Equipment__c WHERE Serial_Number__c LIKE :SerialNumber OR Manufacturer__c LIKE :SerialNumber OR Account__r.name LIKE :SerialNumber OR Model_Name__c LIKE :SerialNumber OR Model_Number__c LIKE :SerialNumber OR Site__r.name LIKE :SerialNumber OR Support_Level__c LIKE :SerialNumber];
        }else{
            //equip=new List<Equipment__c>();
            List<User> userObj = [SELECT id,ContactId FROM USER Where Id =: UserInfo.getUserId()];
           
            if(userObj != null && userObj[0] != null) {
                contactList = [SELECT id,Name,AccountId 
                                             FROM Contact 
                                             WHERE Id = : userObj[0].ContactId];
                
                if(contactList != null && contactList.size() > 0 && contactList[0] != null) {
            System.debug('contactList[0].AccountId else----------'+contactList[0].AccountId);
            equip =[ SELECT Id,Account__r.name,Start_Date__c ,Support_Level__c ,End_Date__c,Equipment_Covered__c,Model_Name__c,Model_Number__c,Name,Purchase_Date__c,Serial_Number__c,Site__r.name,Manufacturer__c FROM Equipment__c WHERE Account__c = :contactList[0].AccountId ORDER BY createdDate desc]; 
                }
            }
        }
        System.debug('equip---.'+equip);
        return equip;
    } 
    @AuraEnabled
    public static List<String> searchForIds(String searchText) {
        
        List<List<SObject>> results = [FIND :searchText IN ALL FIELDS  RETURNING Equipment__c(Serial_Number__c)];
        Equipment__c[] searchEquipment = (Equipment__c[])results[0];
        List<String> SerialNumber = new List<String>();
        for (Equipment__c equip : searchEquipment) {
            SerialNumber.add(equip.Serial_Number__c);
        }
        
        
        return SerialNumber;
    }
     @AuraEnabled
    public static String createRecord (String manufacturer,String SerialNumber,String Subject,String Description){
         Case caseData = new Case(); 
        List<Case> caseList = new List<Case>();
        Integer count=0;
            System.debug('manufacturer-------'+manufacturer);
            System.debug('SerialNumber-------'+SerialNumber);
            System.debug('Subject------------'+Subject);
            System.debug('Description--------'+Description);
        
            List<String> pickListValuesList= new List<String>();
            Schema.DescribeFieldResult fieldResult = Case.Manufacturer__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                if(count==0){
                    if(manufacturer == pickListVal.getLabel()){
                        caseData.Manufacturer__c=manufacturer;
                        count=1;
                    }else{
                        caseData.Manufacturer__c=null;
                    }
                }
            }  
                
                      
            //caseData.Manufacturer__c=manufacturer;
            caseData.Serial_Number__c=SerialNumber;
            caseData.Subject=Subject;
            caseData.Description=Description;
            caseList.add(caseData);
            
            if(caseList.size()>=0){
                Database.SaveResult[] results  = Database.insert(caseList);
                System.debug('***results ****'+results );
            }
            
        
        System.debug('caseList[0].id------------'+caseList[0].id);
        return caseList[0].id;
        
    }    
    
    
    @AuraEnabled
    public static EquipmentDataTableWrapper getEquipmentData(Decimal pageNumber, Integer  recordToDisply) {
        
        pageSize = recordToDisply;
        offset = ((Integer)pageNumber - 1) * pageSize;
         system.debug('pageSize-->' + pageSize);
         system.debug('offset-->' + offset);
        List<User> userObj = [SELECT id,ContactId FROM USER Where Id =: UserInfo.getUserId()];
        EquipmentDataTableWrapper obj =  new EquipmentDataTableWrapper();
        if(userObj != null && userObj[0] != null) {
            List<Contact> contactList = [SELECT id,Name,AccountId 
                                         FROM Contact 
                                         WHERE Id = : userObj[0].ContactId];
            // create a instance of wrapper class.
            
            // set the pageSize,Page(Number), total records and accounts List(using OFFSET)  
            if(contactList != null && contactList.size() > 0 && contactList[0] != null) { 
                obj.pageSize = pageSize;
                obj.page = (Integer) pageNumber;
                obj.offset = offset;
                obj.total = [SELECT count() FROM Equipment__c where  Account__c = :contactList[0].AccountId];
                obj.eqipList = [SELECT Id,Account__r.name,Support_Level__c ,Equipment_Covered__c,Model_Name__c,Model_Number__c,Start_Date__c ,End_Date__c,Name,Purchase_Date__c,Serial_Number__c,Site__r.name,Manufacturer__c FROM Equipment__c where  Account__c = :contactList[0].AccountId ORDER BY createdDate desc,Name LIMIT :recordToDisply OFFSET :offset];
            }
        }
        /*=obj.pageSize = pageSize;
                obj.page = (Integer) pageNumber;
         obj.offset = offset;
                obj.total = [SELECT count() FROM Equipment__c];
         obj.eqipList = [SELECT Id,Account__r.name,Support_Level__c ,Equipment_Covered__c,Model_Name__c,Model_Number__c,Start_Date__c ,End_Date__c,Name,Purchase_Date__c,Serial_Number__c,Site__r.name,Manufacturer__c FROM Equipment__c ORDER BY createdDate desc,Name LIMIT :recordToDisply OFFSET :offset];*/
        // return the wrapper class instance . 
        return obj;
        
    }
    
    @AuraEnabled
   public static list < Equipment__c > fetchContact(String sortField, boolean isAsc,Integer PS,Integer OS)  {
       
       
         system.debug('pageSize-->' + pageSize);
         system.debug('offset-->' + offset);
       system.debug('sortField-->' + sortField);
         system.debug('isAsc-->' + isAsc);
       String accId;
        List < Equipment__c > returnConList = new List < Equipment__c > ();
        List<User> userObj = [SELECT id,email,ContactId FROM USER Where Id =: UserInfo.getUserId()];
       system.debug('userObj-->' + userObj);
        EquipmentDataTableWrapper obj =  new EquipmentDataTableWrapper();
        if(userObj != null && userObj[0] != null) {
            List<Contact> contactList = [SELECT id,Name,AccountId 
                                         FROM Contact 
                                         WHERE Id = : userObj[0].ContactId];
            // create a instance of wrapper class.
            
            // set the pageSize,Page(Number), total records and accounts List(using OFFSET)  
            system.debug('contactList-->' + contactList);
            
                obj.pageSize = PS;
                //obj.page = (Integer) pageNumber;
                obj.offset = OS;
                
                    accId ='\''+contactList[0].AccountId+ '\'';
                
                obj.total = [SELECT count() FROM Equipment__c where  Account__c = :accId];
                //obj.eqipList = [SELECT Id,Account__r.name,Support_Level__c ,Equipment_Covered__c,Model_Name__c,Model_Number__c,Start_Date__c ,End_Date__c,Name,Purchase_Date__c,Serial_Number__c,Site__r.name,Manufacturer__c FROM Equipment__c where  Account__c = :contactList[0].AccountId ORDER BY createdDate desc,Name LIMIT :recordToDisply OFFSET :offset];
                String sSoql = 'SELECT Id,Account__r.name,Support_Level__c ,Equipment_Covered__c,Model_Name__c,Model_Number__c,Start_Date__c ,End_Date__c,Name,Purchase_Date__c,Serial_Number__c,Site__r.name,Manufacturer__c ';
                sSoql += 'From Equipment__c';
                system.debug('sortField-->' + sortField);
                
                if (sortField != '') {
                    sSoql += ' where  Account__c = ' +accId;
                    sSoql += ' order by ' + sortField;
                    
                    // if isAsc is equal tp ture then set 'asc' order otherwise set 'desc' order.
                    if (isAsc) {
                        sSoql += ' asc';
                    } else {
                        sSoql += ' desc';
                    }
                }
                // set record limit to query 
                sSoql += ' LIMIT '+PS+ ' OFFSET '+OS;
                
                System.debug('@Developer -->sSoql:' + sSoql);
                list <Equipment__c> lstResult;
                    system.debug('The query is' + sSoql);
                    lstResult = Database.query(sSoql);
                    system.debug('lstResult' + lstResult);
                   
                    
                    for (Equipment__c c: lstResult) {
                       returnConList.add(c);
                    }
            
        } 
       return returnConList;
       
       
      /*String sSoql = 'SELECT Id,Account__r.name,Support_Level__c ,Equipment_Covered__c,Model_Name__c,Model_Number__c,Start_Date__c ,End_Date__c,Name,Purchase_Date__c,Serial_Number__c,Site__r.name,Manufacturer__c ';
      sSoql += 'From Equipment__c';
      system.debug('sortField-->' + sortField);
 
      if (sortField != '') {
         sSoql += ' order by ' + sortField;
 
    // if isAsc is equal tp ture then set 'asc' order otherwise set 'desc' order.
         if (isAsc) {
            sSoql += ' asc';
         } else {
            sSoql += ' desc';
         }
      }
   // set record limit to query 
      sSoql += ' LIMIT '+PS+ 'OFFSET '+OS;
 
      System.debug('@Developer -->sSoql:' + sSoql);
      list <Equipment__c> lstResult;
      try {
         system.debug('The query is' + sSoql);
         lstResult = Database.query(sSoql);
          system.debug('lstResult' + lstResult);
         List < Equipment__c > returnConList = new List < Equipment__c > ();
 
         for (Equipment__c c: lstResult) {
            returnConList.add(c);
          }
         return returnConList;
      } 
      catch (Exception ex) {
         // for handle Exception
         return null;
      }*/
       
   }
   
     @AuraEnabled //get Account Industry Picklist Values
    public static Map<String, String> getPicklistValue(){
        Map<String, String> options = new Map<String, String>();
        //get Account Industry Field Describe
        Schema.DescribeFieldResult fieldResult = Case.Manufacturer__c.getDescribe();
        //get Account Industry Picklist Values
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            //Put Picklist Value & Label in Map
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
     
    //Wrapper Class For Contact DataTable  
    public class EquipmentDataTableWrapper {
         @AuraEnabled public Integer pageSize {get;set;}
    @AuraEnabled public Integer page {get;set;}
    @AuraEnabled public Integer total {get;set;}
        @AuraEnabled public Integer offset {get;set;}
    @AuraEnabled public List<Equipment__c> eqipList {get;set;}
    }
    
    
    
}