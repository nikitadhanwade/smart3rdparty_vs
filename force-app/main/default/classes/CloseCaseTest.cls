@istest
public class CloseCaseTest {

     @isTest public static void updateStatus(){
        Test.startTest();
         Case caseRecord = new Case();
        caseRecord.Status='Close';
         insert caseRecord;
         
        String options = CloseCase.createRecord(caseRecord.Status,caseRecord.Id);
        Test.stopTest();
    }
}