public class Non3rdPartyMailSender {
    
    @AuraEnabled
    public static list < WrapperClass > fetchAsset() {
        List<WrapperClass> listofWrapper = new List<WrapperClass>();
        String textLabel,url;
        String[] arrTest;
        
        List<User> userObj = [SELECT id,ContactId FROM USER Where Id =: UserInfo.getUserId()];
        
        if(userObj != null && userObj.size() > 0 && userObj[0] != null) {
            List<Contact> contactList = [SELECT id,Name,Account.Id 
                                         FROM Contact 
                                         WHERE Id = : userObj[0].ContactId];
            if(contactList != null && contactList.size() > 0 && contactList[0] != null) {
                
                for(Equipment__c equipmentRecord : [SELECT Id,Name,Account__r.name,Non_Smart3rdParty_Asset__c,Equipment_Covered__c,Model_Number__c,Serial_Number__c,Status__c,Start_Date__c,End_Date__c
                        FROM Equipment__c
                        WHERE Account__r.id = :contactList[0].Account.Id and Non_Smart3rdParty_Asset__c=true]){
                            
                            if(equipmentRecord.Status__c != null){
                                textLabel=equipmentRecord.Status__c;
                                arrTest = textLabel.split('<');
                                String[] abc= arrTest[1].split('"');
                                System.debug('abc----'+abc);
                                System.debug('arrTest----'+arrTest);
                                WrapperClass non3rdPartyWrapper= new WrapperClass(arrTest[0],abc[1],equipmentRecord); 
                                listofWrapper.add(non3rdPartyWrapper);
                                System.debug('listofWrapper----'+listofWrapper);
                            }else{
                                WrapperClass non3rdPartyWrapper= new WrapperClass('','',equipmentRecord); 
                                listofWrapper.add(non3rdPartyWrapper);
                            }
                }
                
                
            }else{
                for(Equipment__c equipmentRecord : [SELECT Id,Name,Account__r.name,Non_Smart3rdParty_Asset__c,Equipment_Covered__c,Model_Number__c,Serial_Number__c,Status__c,Start_Date__c,End_Date__c
                        FROM Equipment__c
                        WHERE Non_Smart3rdParty_Asset__c=true]){
                            
                            if(equipmentRecord.Status__c != null){
                                textLabel=equipmentRecord.Status__c;
                                arrTest = textLabel.split('<');
                                String[] abc= arrTest[1].split('"');
                                System.debug('abc----'+abc);
                                System.debug('arrTest----'+arrTest);
                                WrapperClass non3rdPartyWrapper= new WrapperClass(arrTest[0],abc[1],equipmentRecord); 
                                listofWrapper.add(non3rdPartyWrapper);
                                System.debug('listofWrapper----'+listofWrapper);
                            }else{
                                WrapperClass non3rdPartyWrapper= new WrapperClass('','',equipmentRecord); 
                                listofWrapper.add(non3rdPartyWrapper);
                            }
                            
                            
                            
               
        }
            }
        }
            
        
        return listofWrapper;
        
    }
    @AuraEnabled
 public static String non3rdPartySendMail(List < String > lstRecordId,String toDate,String fromDate,String requestDescription) {
 	 String email= !String.isBlank(system.Label.Non_Smart3rdParty_Email) ? system.Label.Non_Smart3rdParty_Email :'apatil@vieosolutions.com' ;
     Set<String> serialNo = new Set<String>();
     System.debug('lstRecordId----'+lstRecordId);
     System.debug('toDate----'+toDate);
     System.debug('fromDate----'+fromDate);
     
     
     try{
     List<Equipment__c> listOfEquipment= [SELECT Id,Name,Account__r.name,Non_Smart3rdParty_Asset__c,Equipment_Covered__c,Model_Number__c,Serial_Number__c,Start_Date__c,End_Date__c
                        FROM Equipment__c
                        WHERE Id IN :lstRecordId];
     System.debug('listOfEquipment----'+listOfEquipment);
     
     for(Equipment__c equip : listOfEquipment){
         serialNo.add(equip.Serial_Number__c);
     }
     System.debug('serialNo----'+serialNo);
     
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            mail.setToAddresses(new String[] {email});
            mail.setReplyTo('batch@vieosolutions.com');
            mail.setSenderDisplayName('');
            mail.setSubject('Asset record file');
            mail.setPlainTextBody('Support from date:' + fromDate +'\n'+ 'Support to date :'+ toDate +'\n'+ 'Serial Number :'+serialNo +'\n'+ 'Request Description :'+requestDescription);
            //It will send total number of records are deleted by batch
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
          return 'Mail send successfully';
     }catch(Exception e){
         return 'There is an issue';
     }
   
 
 }
    public class WrapperClass{
        @AuraEnabled
        String textLabel{get;set;}
        @AuraEnabled
        String url{get;set;}
        @AuraEnabled
        Equipment__c equipment{get;set;}
        
        public WrapperClass(String textLabel,String url,Equipment__c equipment){
           
            this.textLabel=textLabel;
             this.url=url;
            this.equipment=equipment;
        }
    }
    
}