public class RenewAgreementCon{

    public PageReference CancelAgreement() {
        return new PageReference('/'+objPO.id);
    }


public class wrapperasset{
    public Equipment__c objE{get;set;}
    public boolean isselect{get;set;}
    
}        
    public list<wrapperasset> lstwrapperAssest{get;set;}
    public Purchase_Order__c  objPO{get;set;}
    public Purchase_Order__c   objnewPO{get;set;}
    public RenewAgreementCon(){
    objnewPO = new Purchase_Order__c();
        lstwrapperAssest = new list<wrapperasset>();
        wrapperasset objwrapperasset;
         objPO=[SELECT Contract_End_Date__c,Contract_Start_Date__c,CreatedById,CreatedDate,Data_Migration_Exception__c,DocParser_Document_ID__c,DocParser_Filename__c,DocParser_Remote_ID__c,Equipment_Covered__c,Exception_Description__c,Has_DocParser_Filename__c,Has_Link_to_Doc__c,Id,IsDeleted,Item__c,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Link_to_PDF__c,Manufacturer__c,Model_Name__c,Model_Number__c,Name,Notes__c,Onsite_Included__c,PO_Number__c,Reseller_Address_text__c,Reseller_City__c,Reseller_from_customer_record__c,Reseller_Name_text__c,Reseller_Postal_Code__c,Reseller_State__c,Reseller__c,Serial_Number__c,Ship_To_Address_text__c,Ship_to_Address__c,Ship_To_City_State_Zip__c,Ship_To_City__c,Ship_To_Customer_text__c,Ship_to_Customer__c,Ship_To_Postal_Code__c,Ship_To_State__c,Status__c,Support_Level_Sub_Type__c,Support_Level__c,SystemModstamp FROM Purchase_Order__c where id=:apexpages.currentpage().getparameters().get('id')];
        for(Equipment__c obj : [SELECT Account__c,ATTN__c,Contact__c,CreatedById,CreatedDate,Description__c,DocParser_Document_ID__c,DocParser_Filename__c,DocParser_Remote_ID__c,End_Date__c,Equipment_Covered__c,Id,IsDeleted,Item__c,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Manufacturer__c,Model_Name__c,Model_Number__c,Name,Onsite_Included__c,PO_Number_txt__c,Purchase_Date__c,Related_PO__c,Serial_Number__c,Site__c,Start_Date__c,Support_Level__c,SystemModstamp FROM Equipment__c where Related_PO__c=:apexpages.currentpage().getparameters().get('id') ]){
            objwrapperasset = new wrapperasset();
            objwrapperasset.objE=obj;
            if(test.isrunningtest()){
                objwrapperasset.isselect=true;
            }
            lstwrapperAssest.add(objwrapperasset);
            
        }
        
    }
    public pagereference SaveAgreement(){
           
        if(objnewPO.Contract_Start_Date__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Renewal Start Date'));
    return null;
        }
        else if(objnewPO.Contract_End_Date__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Renewal End Date'));
    return null;
        }
        else if(objnewPO.Contract_Start_Date__c > objnewPO.Contract_End_Date__c){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Renewal End date should be greater than Renewal Start date'));
    return null;
        }
        else if(objnewPO.Contract_Start_Date__c < objPO.Contract_End_Date__c){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Renewal start date should be greater than Contract end date'));
    return null;
        }
        else{
        Purchase_Order__c objnewPOtemp = objPO.clone();
        objnewPOtemp.Contract_Start_Date__c= objnewPO.Contract_Start_Date__c;
        objnewPOtemp.Contract_End_Date__c=objnewPO.Contract_End_Date__c;
        objnewPOtemp.Link_to_PDF__c= objnewPO.Link_to_PDF__c;
        objnewPOtemp.PO_Number__c=objnewPO.PO_Number__c;
        insert objnewPOTemp;
        objPO.Renewal_Date__c =date.today();
        update objpo;
        //Equipment__c objNewE;
        list<Equipment__c> lstE= new list<Equipment__c>();
        for(wrapperasset objwrap:lstwrapperAssest){
            if(objwrap.isselect){
                //objNewE = objwrap.ObjE.clone();
                
                //objnewE.Related_PO__c=objnewPO.id;
                objwrap.objE.Related_PO__c=objnewPOTemp.id;
                objwrap.objE.Start_Date__c=objnewPO.Contract_Start_Date__c;
                objwrap.objE.End_Date__c=objnewPO.Contract_End_Date__c;
                lstE.add(objwrap.objE);
            }
        }
        if(lstE.size()>0){
            update lstE;
        }
        return new pagereference('/'+objnewPOTemp.id);
        }
        }
        
    
}