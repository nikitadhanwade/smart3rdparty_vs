@isTest
public class RenewAgreementConTest{
    @isTest
    public static void testMethod1() {
    Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        Purchase_Order__c objPO= new Purchase_Order__c();
        objPO.Ship_to_Customer__c= accobj.id;
        insert objPO;
        Equipment__c equipmentObj = new Equipment__c();
        equipmentObj .Account__c = accobj.id;
        equipmentObj .Related_PO__c= objPO.id;
        insert equipmentObj; 
        Apexpages.currentpage().getparameters().put('id',objPO.id);
        RenewAgreementCon obj= new RenewAgreementCon();
        RenewAgreementCon.wrapperasset objwrap = new RenewAgreementCon.wrapperasset();
        objwrap.objE = equipmentObj;
        objwrap.isselect=true;
       
        obj.objnewPO = new Purchase_Order__c(Contract_Start_Date__c =date.today(),Contract_End_Date__c  = date.today()+1);
        obj.SaveAgreement();
        obj.CancelAgreement();
    }}