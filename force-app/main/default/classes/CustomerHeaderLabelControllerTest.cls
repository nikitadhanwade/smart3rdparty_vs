@istest
public class CustomerHeaderLabelControllerTest {

    @istest public static void getResellerNameTest(){
        Id p = [select id from profile where name='S3P Customer Community Plus Login User'].id;
        
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = accObj.id;
        insert conObj;
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = conObj.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        system.runAs(user) {
           CustomerCommunityHeaderLabelController.getResellerName();
        }
    }
}