@istest
public class Non3rdPartyMailSenderTest {
    
    @istest public Static void getRecordTest(){
        
        Account acc = new Account();
        acc.Name='TestAcc';
        insert acc;
        Equipment__c equipmentData = new Equipment__c();
        equipmentData.Non_Smart3rdParty_Asset__c=true;
        equipmentData.Description__c='Testing';
        equipmentData.Serial_Number__c='ASCF1234';
        equipmentData.Account__c=acc.Id;
        equipmentData.End_Date__c=date.today()-1;
        insert equipmentData;
        
        Non3rdPartyMailSender non3rdParty = new Non3rdPartyMailSender();
        Non3rdPartyMailSender.fetchAsset();
        
    }
    @istest public Static void getRecordTest1(){
        
        Account acc = new Account();
        acc.Name='TestAcc';
        insert acc;
        Equipment__c equipmentData = new Equipment__c();
        equipmentData.Non_Smart3rdParty_Asset__c=true;
        equipmentData.Description__c='Testing';
        equipmentData.Serial_Number__c='ASCF1234';
        equipmentData.Account__c=acc.Id;
        
        insert equipmentData;
        
        Non3rdPartyMailSender non3rdParty = new Non3rdPartyMailSender();
        Non3rdPartyMailSender.fetchAsset();
        
    }
    @istest public Static void getRecordTest2(){
        
        Id p = [select id from profile where name='S3P Customer Community Login User'].id;
        Account acc = new Account();
        acc.Name='TestAcc';
        insert acc;
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = acc.id;
        insert conObj;
        Equipment__c equipmentData = new Equipment__c();
        equipmentData.Non_Smart3rdParty_Asset__c=true;
        equipmentData.Description__c='Testing';
        equipmentData.Serial_Number__c='ASCF1234';
        equipmentData.Account__c=acc.Id;
        
        insert equipmentData;
        
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = conObj.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        system.runAs(user) {
            Non3rdPartyMailSender.fetchAsset();
        }
        
    }
    @istest public Static void getRecordTest3(){
        
        Id p = [select id from profile where name='S3P Customer Community Login User'].id;
        Account acc = new Account();
        acc.Name='TestAcc';
        insert acc;
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = acc.id;
        insert conObj;
        Equipment__c equipmentData = new Equipment__c();
        equipmentData.Non_Smart3rdParty_Asset__c=true;
        equipmentData.Description__c='Testing';
        equipmentData.Serial_Number__c='ASCF1234';
        equipmentData.End_Date__c=date.today()-1;
        equipmentData.Account__c=acc.Id;
        
        insert equipmentData;
        
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = conObj.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        system.runAs(user) {
            Non3rdPartyMailSender.fetchAsset();
        }
        
    }
    
    @istest public static void non3rdPartySendMailTest(){
        
        Account acc = new Account();
        acc.Name='TestAcc';
        insert acc;
        
        List<Equipment__c> listOfEquipment = new List<Equipment__c>();
        List<String> listOfIds = new List<String>();
        
        Equipment__c equipmentData = new Equipment__c();
        equipmentData.Non_Smart3rdParty_Asset__c=true;
        equipmentData.Description__c='Testing';
        equipmentData.Serial_Number__c='ASCF1234';
        equipmentData.End_Date__c=date.today()-1;
        equipmentData.Account__c=acc.Id;
        listOfEquipment.add(equipmentData);
        
        Equipment__c equipmentData1 = new Equipment__c();
        equipmentData1.Non_Smart3rdParty_Asset__c=true;
        equipmentData1.Description__c='Testing2';
        equipmentData1.Serial_Number__c='TYUI2345';
        equipmentData1.End_Date__c=date.today()-1;
        equipmentData1.Account__c=acc.Id;
        listOfEquipment.add(equipmentData1);
        
        Equipment__c equipmentData2 = new Equipment__c();
        equipmentData2.Non_Smart3rdParty_Asset__c=true;
        equipmentData2.Description__c='Testing';
        equipmentData2.Serial_Number__c='VBNM7890';
        equipmentData2.End_Date__c=date.today()-1;
        equipmentData2.Account__c=acc.Id;
        listOfEquipment.add(equipmentData2);
        
        if(listOfEquipment.size()>0){
            insert listOfEquipment;
        }
        for(Equipment__c equipmentRecord : listOfEquipment){
            listOfIds.add(equipmentRecord.id); 
        }
        Non3rdPartyMailSender.non3rdPartySendMail(listOfIds, String.valueOf(date.today()), String.valueOf(date.today()), 'Testing');
    }
     
}