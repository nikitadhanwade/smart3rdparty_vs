@isTest
public with sharing class ContractExt_Test {

    public static testMethod void doTest() {
    	// Setup some test data
    	Account badger = new Account( Name = 'Badger' );
    	insert badger;
    	Contact badger2 = new Contact( LastName = 'Badger', AccountId = badger.Id );
    	insert badger2;
    	Account serenity = new Account( Name = 'Test Reseller' );
    	insert serenity;
    	Contact c = new Contact( FirstName = 'Malcolm', LastName = 'Reynolds', AccountId = serenity.Id );
    	insert c;
		Schema.DescribeSObjectResult r = Account.SObjectType.getDescribe();
		ID customerAccountRtId = r.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
		ID resellerAccountRtId = r.getRecordTypeInfosByname().get('Reseller').getRecordTypeId();
    	
    	// Now it's time to create an estimate.
    	PageReference pageRef = Page.PO_Entry;
    	Test.setCurrentPageReference( pageRef );
    	contractExt theExt = new ContractExt( new ApexPages.standardController( new Purchase_Order__c() ) );
    	theExt.reseller = badger;
    	theExt.customer = serenity;
    	theExt.newLine();

    	theExt.saveReseller();
    	theExt.saveCustomer();
    	theExt.thePO = null;
    	theExt.newLine();
    	theExt.saveAndNew();
    	
    	theExt.queryAll( 'Purchase_Order__c' );
    	
    	// This should succeed
    	//theExt.saveRecord();

    	//theExt.newLine();
    }
}