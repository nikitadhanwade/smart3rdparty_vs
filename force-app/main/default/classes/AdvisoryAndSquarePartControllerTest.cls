@istest
public class AdvisoryAndSquarePartControllerTest {
    
    @istest static void getCaseRecordsTest(){
        
        Account acc = new Account();
        acc.Advisory__c='Testing demo';
        acc.Spare_Part__c='Sample Data';
        acc.Name='Testing account';
        insert acc;
        
        Case caseRecord = new case();
        caseRecord.AccountId=acc.Id;
        insert caseRecord;
        
        Test.startTest();
        ApexPages.StandardController sc1 = new ApexPages.StandardController(caseRecord);
        AdvisoryAndSquarePartController oppKeys1 = new AdvisoryAndSquarePartController(sc1);
        
        oppKeys1.squarePartMethod();
        oppKeys1.advisoryMethod();
        oppKeys1.closePopup();
        Test.stopTest();
    }
}