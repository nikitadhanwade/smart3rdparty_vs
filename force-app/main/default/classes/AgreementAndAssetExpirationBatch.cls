global class AgreementAndAssetExpirationBatch implements database.Batchable<sObject>{
    
    String query;
    global AgreementAndAssetExpirationBatch(){
        this.query='SELECT Name,(select Id,Name,ExpiresCount__c from Customer_Purchase_Orders__r),(select Name,Related_PO__c from Assets__r),(select email,Send_Email__c from Contacts) FROM Account';
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc,List<Account> acc){
        // System.debug('account-------'+acc);
        Map<id,List<Equipment__c>> agreementIdVsAssetMap = new Map<id,List<Equipment__c>>();
        Map<id,List<Purchase_Order__c>> accountIdVsAgreementMap = new Map<id,List<Purchase_Order__c>>();
        Map<id,List<String>> accountIdVsContactEmailMap = new Map<id,List<String>>();
        
        Map<String,List<Account>> booleanVsAccountMap = new Map<String,List<Account>>();
        List<Account> accountListForProcede = new List<Account>();
        for(Account accRecord : acc){
            boolean isValid = false;
            for(Contact con : accRecord.Contacts){
                if(con.email != null && con.Send_Email__c){
                    if(accountIdVsContactEmailMap.containsKey(accRecord.id)){
                        List<String> contactEmail=accountIdVsContactEmailMap.get(accRecord.id);
                        contactEmail.add(con.email);
                        accountIdVsContactEmailMap.put(accRecord.id,contactEmail);
                    }else{
                        accountIdVsContactEmailMap.put(accRecord.id,new List<String>{con.email});
                    }
                    isValid= true; 
                }
                
            }
            if(isValid){
                accountListForProcede.add(accRecord);
            }
        }
         //System.debug('accountListForProcede------'+accountListForProcede.size());
        
        for(Account accRecord : accountListForProcede){
            for(Purchase_Order__c agreementObject :accRecord.Customer_Purchase_Orders__r){
                if(accountIdVsAgreementMap.containsKey(accRecord.Id)){
                    List<Purchase_Order__c> agreementList = accountIdVsAgreementMap.get(accRecord.Id);
                    agreementList.add(agreementObject);
                    accountIdVsAgreementMap.put(accRecord.Id,agreementList);   
                }else{
                    accountIdVsAgreementMap.put(accRecord.Id, new List<Purchase_Order__c>{agreementObject});
                }
            } 
            
            List<Purchase_Order__c> purchaseOrderList= new List<Purchase_Order__c>();
            List<Id> agreementId = new List<Id>();
            For( List<Purchase_Order__c> purchaseList : accountIdVsAgreementMap.values()){
                purchaseOrderList.addAll(purchaseList);
            }
            for(Purchase_Order__c agreementIdRecord: purchaseOrderList){
                agreementId.add(agreementIdRecord.Id);
            }
            
            for(Equipment__c equipAsset :accRecord.Assets__r){
                if(agreementId.contains(equipAsset.Related_PO__c)){
                    if(agreementIdVsAssetMap.containsKey(equipAsset.Related_PO__c)){
                        List<Equipment__c> agreementList = agreementIdVsAssetMap.get(equipAsset.Related_PO__c);
                        agreementList.add(equipAsset);
                        agreementIdVsAssetMap.put(equipAsset.Related_PO__c,agreementList);   
                    }else{
                        agreementIdVsAssetMap.put(equipAsset.Related_PO__c,new List<Equipment__c>{equipAsset});
                    }
                    
                }
            }
        }
        
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        System.debug('emailList1232-----'+emailList);
        
        String emailSubject,emailBodyData;
        Boolean isExpire=false;
        for(Account accRecord : accountListForProcede){
            List<String> emailBodyList = new List<String>();
            if(accountIdVsAgreementMap.containsKey(accRecord.id)){
                List<String> listOfContact= accountIdVsContactEmailMap.get(accRecord.id);
                List<Purchase_Order__c> listOfAgreements= accountIdVsAgreementMap.get(accRecord.id);
                System.debug('listOfAgreements----'+listOfAgreements); 
                System.debug('listOfAgreements size----'+listOfAgreements.size());
                
                for(Purchase_Order__c purchaseOrderRecord : listOfAgreements){
                    List<Equipment__c> assetList = agreementIdVsAssetMap.get(purchaseOrderRecord.id);
                    integer i = Integer.valueOf(purchaseOrderRecord.ExpiresCount__c);
                    if(i == 0 || i == 30 || i == 60 || i == 90){
                        isExpire=true;
                    }
                }
                if(isExpire){
                    emailSubject = '\n\n'+'Agreement and Asset details for '+'Customer Name :-' +accRecord.Name +'\n\n';
                    emailBodyList.add(emailSubject);  
                }
                
                
                for(Purchase_Order__c agreementRecord : listOfAgreements){ 
                    List<Equipment__c> assetList = agreementIdVsAssetMap.get(agreementRecord.id);
                    integer i = Integer.valueOf(agreementRecord.ExpiresCount__c);
                    System.debug('agreementRecord----'+agreementRecord);
                    System.debug('i----'+i);
                      System.debug('assetList----'+assetList);
                    
                     if(i == 0){
                        emailSubject = '\n\n' + 'Agreements and assets expiring today :-'+'\n' + 'Agreements Details :-' +agreementRecord.Name;
                        emailBodyList.add(emailSubject);
                    }else if(i == 30){
                        emailSubject = '\n\n' + 'Agreements and assets expiring in 30 days :-'+'\n'  + 'Agreements Details :-' +agreementRecord.Name;
                        emailBodyList.add(emailSubject);
                    }else if(i == 60){
                        emailSubject = '\n\n' + 'Agreements and assets expiring in 60 days :-'+'\n'+ 'Agreements Details :-' +agreementRecord.Name;
                        emailBodyList.add(emailSubject);
                    }else if(i == 90){
                        emailSubject = '\n\n' + 'Agreements and assets expiring in 90 days :-'+'\n'  + 'Agreements Details :-' +agreementRecord.Name;
                        emailBodyList.add(emailSubject);
                    }
                    
                    if(assetList != null){
                        for(Equipment__c aseetRecord : assetList){
                            if(i == 0){
                                emailSubject ='\n' + 'Asset Details :-' +aseetRecord.Name;      
                                emailBodyList.add(emailSubject);
                            }else if(i == 30){
                                emailSubject ='\n' + 'Asset Details :-' +aseetRecord.Name;      
                                emailBodyList.add(emailSubject);
                            }else if(i == 60){
                                emailSubject ='\n' + 'Asset Details :-' +aseetRecord.Name;      
                                emailBodyList.add(emailSubject);
                            }else if(i == 90){
                                emailSubject ='\n' + 'Asset Details :-' +aseetRecord.Name;      
                                emailBodyList.add(emailSubject);
                            }
                            
                        }//End Asset for loop   
                    }
                    
                    
                } // End Agreement For loop
                System.debug('emailBodyList--'+emailBodyList);
                if(emailBodyList.size()>0 && emailBodyList !=null){
                    for(integer i=0;i<emailBodyList.size();i++){
                        If(emailBodyData == null)
                        {
                            emailBodyData = emailBodyList[i]; 
                        }
                        Else{
                            emailBodyData = emailBodyData  + ' '+emailBodyList[i];
                        }
                    }
                    System.debug('emailBodyData--'+emailBodyData);
                    System.debug('listOfContact--'+listOfContact);
                    //String emailBody = string.valueOf(emailBodyList);
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(listOfContact);
                    mail.setReplyTo('batch@vieosolutions.com');
                    mail.setSenderDisplayName('Agreement and Asset Expirations Processing');
                    mail.setSubject('Agreement and Asset Expirations Process Completed');
                    mail.setPlainTextBody(emailBodyData);
                    System.debug('mail' + mail);
                    emailList.add(mail);
                    emailBodyData=null;
                }
            }     
        }
        
        System.debug('emailList-----'+emailList);
        if(emailList.size()>0 && emailList != null){
            Messaging.sendEmail(emailList);
        }
        
        System.debug('accountIdVsContactEmailMap-------'+accountIdVsContactEmailMap);
         System.debug('accountIdVsAgreementMap-------'+accountIdVsAgreementMap);
        System.debug('agreementIdVsAssetMap-------'+agreementIdVsAssetMap);
    }
    global void finish(Database.BatchableContext bc){
        
    }
}