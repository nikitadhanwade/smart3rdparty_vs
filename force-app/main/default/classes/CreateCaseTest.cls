@isTest
public class CreateCaseTest {
    
    @testSetup
    static void methodName() {
        
        Account acc = new Account();
        acc.name='Test';
        insert acc;
        
        Equipment__c equipment = new Equipment__c();
        equipment.End_Date__c=system.today();
        equipment.Purchase_Date__c=system.today();
        equipment.Serial_Number__c='ABC007';
        equipment.Manufacturer__c='Dell';
        equipment.Account__c=acc.Id;
        insert equipment;
    }
    
    @isTest public static void getAllEquipment(){
        CreateCase.getAllSerialNumber();
    }
    @isTest public static void getSerachElement(){
        Equipment__c equip = [Select Serial_Number__c from Equipment__c where Manufacturer__c = 'Dell'];
        Id p = [select id from profile where name='S3P Customer Community Login User'].id;
        
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = accObj.id;
        insert conObj;
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = conObj.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        system.runAs(user) {
            CreateCase.findSerialNumber(equip.Serial_Number__c);
        }
    }
    @isTest public static void getSerachElementIsNull(){
        Equipment__c equip = [Select Serial_Number__c from Equipment__c where Manufacturer__c = 'Dell'];
        Id p = [select id from profile where name='S3P Customer Community Login User'].id;
        
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = accObj.id;
        insert conObj;
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = conObj.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        system.runAs(user) {
            CreateCase.findSerialNumber('');
        }
    }
    @isTest public static void getAllSerachElement(){
        Equipment__c equip = [Select Serial_Number__c from Equipment__c where Manufacturer__c = 'Dell'];
        CreateCase.searchForIds(equip.Serial_Number__c);
    }
    @isTest public static void createCase(){
        CreateCase.createRecord('Dell', 'PQR001', 'Demo', 'Testing');
    }
    @isTest public static void getEquipmentDataTest(){
        Id p = [select id from profile where name='S3P Customer Community Login User'].id;
        
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = accObj.id;
        insert conObj;
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = conObj.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        system.runAs(user) {
            CreateCase.getEquipmentData(1,10);
        }
    }
    @isTest public static void getfetchContact(){
        
        Id p = [select id from profile where name='S3P Customer Community Login User'].id;
        
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = accObj.id;
        insert conObj;
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = conObj.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        system.runAs(user) {
            CreateCase.fetchContact('Manufacturer__c', false, 10, 0);
        }
        
    }
    @isTest public static void getPickListValue(){
        Test.startTest();
        Map<String, String> options = CreateCase.getPicklistValue();
        Test.stopTest();
    }
}