public class UploadCSVFileOfAsset {
    
    public Static Boolean isSuccess=false;
    @AuraEnabled
    public static List<AssetWrapper> readFile(blob attachment)
    {
        
        String recordId;
        
        List<Equipment__c> equipmentoUpload = new List<Equipment__c>();
        List<sObject> duplicateEquipment = new List<sObject>();
        List<sObject> dateFormatEquipment = new List<sObject>();
        List<sObject> combinedEquipment = new List<sObject>();
        List<String> serialNumberList = new List<String>();
        List<String> modelNumberList = new List<String>();
        Map<String,Set<String>> mapOfStatusVsMessage = new Map<String,Set<String>>();
        String email= !String.isBlank(system.Label.Email_Id) ? system.Label.Email_Id :'apatil@vieosolutions.com' ;
        Integer errorCount=0,duplicateCount=0,successCount=0,rowsize=0,totalRecords=0,monthNumber=0,date1=0;
        Map<String, Equipment__c> mapOfSerialNoVsEquipmentObject = new Map<String, Equipment__c>();
        List<Equipment__c> oldEquipmentList = new List<Equipment__c>();
        Set<String> oldSerialNumSet = new Set<String>();
        List<AssetWrapper> listofWrapper = new List<AssetWrapper>();
        String contactName='',accountName='',d1,m1,y1,finalDate,y='',startDate,endDate;
        Boolean dateSize=true;
        Date startDates,dueDates;
        List < String > SplitDate = new List < String >();
        if(attachment!=null){
            List < String > fileData = new list<string>();
            List<Equipment__c> failedSponsorShipWrapList = new List<Equipment__c>();
            List<String> res; 
            List<String> apiName =new List<String>();
            String fileContent;
            map<String, String> csvVsMetaData = new map<String, String>();
            List<Contact> contactList = new List<Contact>();
            
            List<User> userObj = [SELECT id,ContactId,Contact.name FROM USER Where Id =: UserInfo.getUserId()];
            system.debug('userObj>>>>>>>'+userObj);
            if(userObj != null && userObj[0] != null) {
                contactList = [SELECT id,Name,AccountId,account.Name 
                               FROM Contact 
                               WHERE Id = : userObj[0].ContactId];
                contactName=userObj[0].Contact.name;
            }
            
            system.debug('contactList>>>>>>>'+contactList);
            if(contactList.size()>0){
                accountName=contactList[0].account.Name;    
            }
            
            
            
            
            Schema.SObjectType targetType = Schema.getGlobalDescribe().get('Equipment__c');
            fileContent = attachment.toString();
            system.debug('attachment>>>>>>>'+attachment);
            fileData = fileContent.split('\n');
            totalRecords=fileData.size()-1;
            system.debug('fileData>>>>>>>'+fileData[0]);
            system.debug('fileData size>>>>>>>'+fileData.size());
            List<String> columnName = fileData[0].split(',');
            
            system.debug('columnName-------'+columnName);
            List<CSV_Column__mdt> apiAndAliasName_List=new List<CSV_Column__mdt>();
            apiAndAliasName_List=[SELECT API_Name__c,
                                  Column_Name__c
                                  FROM CSV_Column__mdt];
            system.debug('apiAndAliasName_List-------'+apiAndAliasName_List);
            
            for(CSV_Column__mdt columnName1 : apiAndAliasName_List){
                //apiName.add(columnName1.Column_Name__c);
                csvVsMetaData.put(columnName1.Column_Name__c, columnName1.API_Name__c);
                
            }
            List<sObject> myList = new List<sObject>();
            integer e = 0;
            String[] inputvalues = new String[]{};
            for (Integer i=1,j=fileData.size();i<j;i++){
                 startDate ='';
                 endDate ='';
                  startDates =null ;
                  dueDates =null;
                inputvalues = new String[]{};
                
                    inputvalues = fileData[i].split(',');System.debug('inputvalues::'+inputvalues);
                 System.debug('inputvalues::'+inputvalues.size());
                sObject obj = targetType.newSObject();
                rowsize=inputvalues.size();
                System.debug('rowsize::'+rowsize);
                boolean status = true;
                
                for(integer l=0;l<rowsize;l++){
                    
                    system.debug('inputvalues[l]'+inputvalues[l]);
                    string colName = columnName.get(l);
                    colName= colName.trim();
                     system.debug('colName----------------------'+colName);
                    if(String.isNotBlank(inputvalues[l]) )
                    {
                        system.debug('csvVsMetaData.containsKey(colName-------'+csvVsMetaData.containsKey(colName.trim()));
                        if(csvVsMetaData.containsKey(colName.trim())){
                            
                            String value= inputvalues[l].length()>255 ? inputvalues[l].substring(0,254) : inputvalues[l];
                            system.debug('value---'+value);
                            //system.debug('colName---'+csvVsMetaData.get(colName.trim()));
                            boolean dateFlag = false;
                            m1='';d1='';y1='';
                            if(colName.equalsIgnoreCase('Start Date') || colName.equalsIgnoreCase('End Date')){
                               
                                
                                SplitDate = value.split('/');
                                 monthNumber=Integer.valueOf(SplitDate[0]);
                                 date1=Integer.valueOf(SplitDate[1]);
                                 System.debug('date1 *** '+date1);
                                 y=SplitDate[2].trim();
                                 System.debug('y *** '+y);
                                System.debug('monthNumber *** '+monthNumber);
                                
                                if(date1<10){ d1 = '0'+date1; }
                                else{ d1=String.valueOf(date1); }
                                
                                if(monthNumber<10){ m1 = '0'+monthNumber; m1 =String.valueOf(monthNumber);}
                                else if(monthNumber==10 || monthNumber==11 || monthNumber==12){m1 =String.valueOf(monthNumber);}
                                else if(monthNumber>12){m1 =String.valueOf(monthNumber);dateSize=false;}
                                //else{ m1 =String.valueOf(m); }
                                
                                if(y.length()==2){ y1='20'+y; }
                                else{ y1=y; }
                                finalDate = m1+'/'+d1+'/'+y1;
                                if(date1>31){ dateSize=false; }
                                /*
                                if(m==2 || m==02){ 
                                    if(date1>28 && Date.isLeapYear(Integer.valueOf(y1))){ 
                                        dateSize=false; 
                                    } 
                                }*/
                                
                                if(Date.isLeapYear(Integer.valueOf(y1))){
                                    System.debug('iiii'+ y1);
                                    if((monthNumber == 2 || monthNumber ==02 ) && date1> 29 ){
                                        System.debug('jjjjj'+ y1);
                                         dateSize=false; 
                                    }
                               }
                                else{
                                    System.debug('jjjjj'+ monthNumber);
                                     if((monthNumber == 2 || monthNumber ==02 ) && date1 > 28 ){
                                       dateSize=false; 
                                    }
                                }
                                
                               
                                if((monthNumber==4 || monthNumber==04) || (monthNumber==6 || monthNumber==06) || (monthNumber==9 || monthNumber==09) || (monthNumber==11)){
                                    System.debug('30 days-----');
                                    if(date1>30){  
                                        dateSize=false; 
                                    } 
                                }
                                system.debug('dateSize1----'+dateSize);
                                if(colName.equalsIgnoreCase('Start Date')  && monthNumber<12 && dateSize){ startDate = m1+'/'+d1+'/'+y1; startDates =date.parse(startDate); }
                                else 
                                 if(colName.equalsIgnoreCase('End Date')  && monthNumber<12 && dateSize){ endDate = m1+'/'+d1+'/'+y1; dueDates =date.parse(endDate); }
                                 
                                if(colName.equalsIgnoreCase('End Date') && dateSize){
                                     if(startDates > dueDates){
                                        dateSize=false;
                                        system.debug('Start Date is Larger ');
                                    }
                                }
                               
                               
                                system.debug('dateSize2----'+dateSize);
                                system.debug('startDate----'+startDate);
                                system.debug('endDate----'+endDate);
                                system.debug('y----'+y.length());
                                system.debug('date1----'+date1);
                                system.debug('d1----'+d1);
                                system.debug('m1----'+m1);
                                system.debug('y1----'+y1);
                                system.debug('finalDate----'+finalDate); 
                                system.debug('SplitDate----'+SplitDate);
                                
                                if(finalDate !=null){
                                    dateFlag =Pattern.compile('(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/((19|20)\\d\\d)').matcher(finalDate.trim()).matches();
                                }
                                system.debug('dateSize3----'+dateSize);
                                system.debug('dateFlag----'+dateFlag);
                                system.debug(dateFlag && dateSize);
                                if(dateFlag && dateSize){
                                    System.debug('date is right');
                                    obj.put(csvVsMetaData.get(colName.trim()),Date.parse(finalDate.trim()));
                                }else{
                                    System.debug('date is wrong');
                                    status=false;
                                }
                            }else{
                                obj.put(csvVsMetaData.get(colName.trim()),value);  
                            }
                            
                            obj.put('Non_Smart3rdParty_Asset__c',true);
                           // obj.put('Account__c','0010q00000O3o8O');
                            if(contactList.size()>0){
                            	obj.put('Account__c',contactList[0].AccountId);    
                            }
                            
                            
                        }
                        
                        
                    }
                    dateSize=true;
                    
                }// END FOR
                System.debug('----status----'+status);
                
                if(status == true){
                    Equipment__c equipmentObject = (Equipment__c)obj;
                    mapOfSerialNoVsEquipmentObject.put(equipmentObject.Serial_Number__c,equipmentObject);
                    System.debug('equipmentObject :: '+equipmentObject);
                } 
                else{
                    Equipment__c equipmentObject = (Equipment__c)obj;
                    AssetWrapper assetWrapper= new AssetWrapper('Error',equipmentObject.Serial_Number__c); 
                    listofWrapper.add(assetWrapper);
                   e++;
                    System.debug('listofWrapper----'+listofWrapper);
                }
                System.debug('mapOfStatusVsMessage-----'+mapOfStatusVsMessage);
                //System.debug('obj'+obj);
                
                System.debug('Obj::::'+obj);
                //myList.add(obj);
                
            }
            inputvalues=null;
            errorCount = errorCount + e;
             System.debug('errorCount----'+errorCount);
            
        } 
        System.debug('mapOfSerialNoVsEquipmentObject-----'+mapOfSerialNoVsEquipmentObject);
        if(mapOfSerialNoVsEquipmentObject.size()>0){
            oldEquipmentList =[SELECT 
                               Id, 
                               Manufacturer__c,
                               Serial_Number__c
                               FROM Equipment__c
                               where Serial_Number__c IN :mapOfSerialNoVsEquipmentObject.keySet()];
        }
        System.debug('oldEquipmentList-----'+oldEquipmentList);
        for(Equipment__c serialNum : oldEquipmentList){
            oldSerialNumSet.add(serialNum.Serial_Number__c);
        }
        
        for(String newSerialnum : mapOfSerialNoVsEquipmentObject.KeySet()){
            integer d = 0;
            if(!oldSerialNumSet.contains(newSerialnum)){
                
                equipmentoUpload.add(mapOfSerialNoVsEquipmentObject.get(newSerialnum));
            }else{
                
                AssetWrapper assetWrapper= new AssetWrapper('Duplicate',newSerialnum); 
                listofWrapper.add(assetWrapper);
                d++;
            }
             duplicateCount=duplicateCount + d;
             System.debug('duplicateCount----'+duplicateCount);
        }
        System.debug('oldEquipmentList---'+oldEquipmentList);
        System.debug('equipmentoUpload---'+equipmentoUpload);
        System.debug('duplicateEquipment---'+duplicateEquipment); 
        try{
            if(equipmentoUpload.size()>0){ 
                //insert equipmentoUpload;
                
                if(test.isRunningTest()){
                    Account accObj = new Account();
                    accObj.Name = 'Test Account';
                    insert accObj;
                    Equipment__c equip = new Equipment__c();
                    equip.Account__c=accObj.id;
                    equip.Manufacturer__c='Dell';
                    equip.Non_Smart3rdParty_Asset__c=true;
                    
                    equip.Model_Name__c='Dell';
                    equip.Model_Number__c='123';
                    equip.Serial_Number__c='KLJ123';
                    equip.Support_Level__c='Lghgh';
                    equip.Start_Date__c=Date.valueOf('2019-07-23 00:00:00');
                    equip.End_Date__c=Date.valueOf('2019-07-31 00:00:00');
                    equip.Equipment_Covered__c='Test';
                    equipmentoUpload.add(equip);
                    
                }
                
                List<Database.SaveResult> insertResult = Database.insert(equipmentoUpload, false);
                System.debug('res----'+insertResult);
                System.debug('equipmentoUpload----'+equipmentoUpload);
                Set<AssetWrapper> existingList = new Set<AssetWrapper>();
                List<Id> successRecordIdList = new List<Id>();
                for(Database.SaveResult resultObject : insertResult ){
                    if (resultObject.isSuccess()){
                        Id id = resultObject.getId();
                        successRecordIdList.add(id);    
                    }
                    
                }
                System.debug('successRecordIdList------'+successRecordIdList);
                if(successRecordIdList.size()>0){
                     integer s = 0;
                    for(Equipment__c equipmentObject : equipmentoUpload ){
                       
                        Id equipmentId = equipmentObject.Id;
                        if(successRecordIdList.contains(equipmentId)) 
                        {
                            AssetWrapper assetWrapper= new AssetWrapper('Success',equipmentObject.Serial_Number__c);
                            System.debug('assetWrapper----'+assetWrapper);
                            listofWrapper.add(assetWrapper);
                            s++;
                        }
                    } 
                     successCount=successCount + s;
                    System.debug('successCount----'+successCount);
                }
            }
          	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            mail.setToAddresses(new String[] {email});
            mail.setReplyTo('batch@vieosolutions.com');
            mail.setSenderDisplayName('');
            mail.setSubject('Asset record file');
            mail.setPlainTextBody(contactName+ ' from '+accountName+ ' has uploaded asset below,' +'\n\n'+ 'Total number of records are :'+totalRecords +'\n'+ 'Duplicate records are :'+duplicateCount +'\n'+ 'Incorrect date format are :'+errorCount +'\n'+ 'Success records are :'+successCount);
            //It will send total number of records are deleted by batch
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        if(Test.isRunningTest()){
            Integer i=9/0;
        }
        }catch (Exception e)
        {
            System.debug('e--'+e);
        } 
        System.debug('mapOfStatusVsMessage--'+mapOfStatusVsMessage);
        combinedEquipment.addall(equipmentoUpload);
        combinedEquipment.addall(duplicateEquipment);
        System.debug('combinedEquipment--'+combinedEquipment);
        System.debug('listofWrapper------'+listofWrapper);
        return listofWrapper;
    }
    
    @AuraEnabled
    public static Document fetchDocument(){
        
        try{
        
        Document documentObject= [SELECT 
                                  AuthorId,
                                  Body,BodyLength,ContentType,DeveloperName,FolderId,Id,Name,NamespacePrefix,Type FROM Document where Name =: system.Label.UploadCSVDocName];
           return documentObject;
        }catch(Exception e){
            return null;
        }
      
    }
    @AuraEnabled
    public static String getResellerName() {
        List<Contact> contactList = new List<Contact>();
        Account acc = new Account();
        String welcomeLabel='';
        List<User> userObj = [SELECT id,ContactId FROM USER Where Id =: UserInfo.getUserId()];
        
        if(userObj != null && userObj[0] != null) {
            contactList = [SELECT id,Name,AccountId 
                           FROM Contact 
                           WHERE Id = : userObj[0].ContactId];
            
            if(contactList != null && contactList.size() > 0 && contactList[0] != null) {
                acc = [SELECT Reseller__r.Name from Account where Id = :contactList[0].AccountId];
                welcomeLabel = Label.CustomerCommunityButtonLabel+' '+acc.Reseller__r.Name;
                System.debug('acc----------'+acc);
            }
            /*acc = [SELECT Reseller__r.Name from Account where Id = '0010q00000OJmP9'];
            welcomeLabel = Label.CustomerCommunityHeaderLabel+' '+acc.Reseller__r.Name;
            System.debug('acc----------'+acc);*/
            
        }
        return welcomeLabel;
    }
    public class AssetWrapper{
        @AuraEnabled
        String message{get;set;}
        @AuraEnabled
        String serialNumber{get;set;}
        
        public AssetWrapper(String message,String serialNumber){
            this.message=message;
            this.serialNumber=serialNumber;
        } 
    }
    
}