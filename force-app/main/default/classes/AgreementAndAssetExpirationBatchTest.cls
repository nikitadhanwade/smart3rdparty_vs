@isTest
public class AgreementAndAssetExpirationBatchTest {
    
    static testMethod void testMethod1(){
        
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        
        List<Contact> ConList = New list<Contact>();
        Contact conObj = new Contact();
        conObj.AccountId = accObj.id;
        conObj.Send_Email__c=True;
        conObj.Email='test@email.com';
        conObj.LastName='abc';
        ConList.add(conObj);
        
        Contact conObj1 = new Contact();
        conObj1.AccountId = accObj.id;
        conObj1.Send_Email__c=True;
        conObj1.Email='test1@email.com';
        conObj1.LastName='abc';
        ConList.add(conObj1);
        
        Contact conObj2 = new Contact();
        conObj2.AccountId = accObj.id;
        conObj2.Send_Email__c=false;
        conObj2.Email='test3@email.com';
        conObj2.LastName='abc';
        ConList.add(conObj2); 
        
        Contact conObj3 = new Contact();
        conObj3.AccountId = accObj.id;
        conObj3.Send_Email__c=false;
        conObj3.Email='';
        conObj3.LastName='abc';
        ConList.add(conObj3); 
        insert ConList;
        
        List<Purchase_Order__c> aggList = New list<Purchase_Order__c>();
        Purchase_Order__c aggObj = new Purchase_Order__c();
        aggObj.Reseller__c=accObj.id;
        Date myDate = Date.today();
        Date newDate = mydate.addDays(30);
        aggObj.Contract_End_Date__c =newDate;
        aggObj.Ship_to_Customer__c =accObj.id;
        aggList.add(aggObj);
        
        Purchase_Order__c aggObj1 = new Purchase_Order__c();
        aggObj1.Reseller__c=accObj.id;
        Date myDate1 = Date.today();
        Date newDate1 = myDate1.addDays(60);
        aggObj1.Contract_End_Date__c =newDate1;
        aggObj1.Ship_to_Customer__c =accObj.id;
        aggList.add(aggObj1);
        
        Purchase_Order__c aggObj2 = new Purchase_Order__c();
        aggObj2.Reseller__c=accObj.id;
        Date myDate2 = Date.today();
        Date newDate2 = mydate2.addDays(90);
        aggObj2.Contract_End_Date__c = newDate2;
        aggObj2.Ship_to_Customer__c =accObj.id;
        aggList.add(aggObj2);
        
        Purchase_Order__c aggObj3 = new Purchase_Order__c();
        aggObj3.Reseller__c=accObj.id;
        aggObj3.Contract_End_Date__c = date.today();
        aggObj3.Ship_to_Customer__c =accObj.id;
        aggList.add(aggObj3);
        
        Purchase_Order__c aggObj4 = new Purchase_Order__c();
        aggObj4.Reseller__c=accObj.id;
        aggObj4.Contract_End_Date__c =Date.valueOf('2019-2-18 00:00:00');
        aggObj4.Ship_to_Customer__c =accObj.id;
        aggList.add(aggObj4);
        insert aggList;
        
        
        List<Equipment__c> equipList = New list<Equipment__c>();
        
        Equipment__c equip = new Equipment__c();
        equip.Account__c=accObj.id;
        equip.Model_Name__c='Dell';
        equip.Model_Number__c='123';
        equip.Serial_Number__c='KLJ123';
        equip.Support_Level__c='Lghgh';
        equip.Equipment_Covered__c='Test';
        equip.Related_PO__c=aggObj.id;
        equipList.add(equip);
        
        Equipment__c equip1 = new Equipment__c();
        equip1.Account__c=accObj.id;
        equip1.Model_Name__c='Dell';
        equip1.Model_Number__c='123';
        equip1.Serial_Number__c='KLJ123';
        equip1.Support_Level__c='Lghgh';
        equip1.Equipment_Covered__c='Test';
        equip1.Related_PO__c=aggObj1.id;
        equipList.add(equip1);
        
        Equipment__c equip2 = new Equipment__c();
        equip2.Account__c=accObj.id;
        equip2.Model_Name__c='Dell';
        equip2.Model_Number__c='123';
        equip2.Serial_Number__c='KLJ123';
        equip2.Support_Level__c='Lghgh';
        equip2.Equipment_Covered__c='Test';
        equip2.Related_PO__c=aggObj2.id;
        equipList.add(equip2);
        
        Equipment__c equip3 = new Equipment__c();
        equip3.Account__c=accObj.id;
        equip3.Model_Name__c='Dell';
        equip3.Model_Number__c='123';
        equip3.Serial_Number__c='KLJ123';
        equip3.Support_Level__c='Lghgh';
        equip3.Equipment_Covered__c='Test';
        equip3.Related_PO__c=aggObj3.id;
        equipList.add(equip3);
        
        Equipment__c equip4 = new Equipment__c();
        equip4.Account__c=accObj.id;
        equip4.Model_Name__c='Dell';
        equip4.Model_Number__c='123';
        equip4.Serial_Number__c='KLJ123';
        equip4.Support_Level__c='Lghgh';
        equip4.Equipment_Covered__c='Test';
        equip4.Related_PO__c=aggObj4.id;
        equipList.add(equip4);
        
        insert equipList;
        
        Test.startTest(); 
        
        AgreementAndAssetExpirationBatch aaeb = new AgreementAndAssetExpirationBatch();
        database.executeBatch(aaeb);
        
        Test.stopTest();
        
        
    }
    
}