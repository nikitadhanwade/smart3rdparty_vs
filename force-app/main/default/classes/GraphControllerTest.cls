@isTest
public class GraphControllerTest {
    @isTest
    public static void testCases() {
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        insert accObj;
        Contact conObj = new Contact();
        conObj.LastName = 'Test Contact';
        conObj.AccountId = accObj.id;
        insert conObj;
        /*Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',
                         ContactId = conObj.id);
        System.runAs(u) {*/
        List<Case> caseList = new List<Case>();
        for(integer i = 0;i<6;i++){
            Case caseObj = new Case();
            caseObj.AccountId = accObj.id;
            caseObj.ContactId = conObj.id;
            caseObj.status = 'New Ticket';
            caseList.add(caseObj);
        } 
        insert caseList;
        GraphController.getChartMap();
            System.assertEquals(caseList != null, true);
    }
}