public class EquipmentController {
    
  
    @AuraEnabled
    public static List<Equipment__c> findAll() {
        List<User> userObj = [SELECT id,ContactId FROM USER Where Id =: UserInfo.getUserId()];
        
        if(userObj != null && userObj.size() > 0 && userObj[0] != null) {
            List<Contact> contactList = [SELECT id,Name,Account.Id 
                                     FROM Contact 
                                     WHERE Id = : userObj[0].ContactId];
            if(contactList != null && contactList.size() > 0 && contactList[0] != null) {
                return [SELECT id, name, Site__r.Location__Latitude__s, Site__r.Location__Longitude__s
            			FROM Equipment__c
            			WHERE Site__r.Location__Latitude__s != NULL AND Site__r.Location__Longitude__s != NULL
            			AND  Account__r.id = :contactList[0].Account.Id LIMIT 50];
                
            }
        }
                  return [SELECT id, name, Site__r.Location__Latitude__s, Site__r.Location__Longitude__s
            FROM Equipment__c
            WHERE Site__r.Location__Latitude__s != NULL AND Site__r.Location__Longitude__s != NULL];
                
            
        }
    }