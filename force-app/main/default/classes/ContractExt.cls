public with sharing class ContractExt {

	public Purchase_Order__c thePO {get;set;}
	public List<Equipment__c> poLines {get;set;}
	public Account reseller {get;set;}
	public Account customer {get;set;} 
	public ApexPages.StandardController myController {get;set;}
	private Id customerAccountRtId;
	private Id resellerAccountRtId;
	public string foundCustomer {get;set;}
	public string foundReseller {get;set;}
	
    public ContractExt( ApexPages.StandardController controller ) {
    	myController = controller;
    	if( null == controller.getRecord().Id ) {
    		thePO = new Purchase_Order__c();
    		System.debug( 'Creating a new PO' );
    		reseller = new Account();
    		customer = new Account();
    	} else {
    		//String query = 'SELECT ' + queryAll('Purchase_Order__c') + ' WHERE Id = \'' + controller.getRecord().Id + '\'';
    		//thePO = database.query(query);
    		// This doesn't work, anyway. 
    		thePO = (Purchase_Order__c)controller.getRecord();
    		System.debug( 'Trying to use an existing PO' );
    	}

		// Example of how to get RecordTypes without SOQL
		Schema.DescribeSObjectResult r = Account.SObjectType.getDescribe();
		customerAccountRtId = r.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
		resellerAccountRtId = r.getRecordTypeInfosByname().get('Reseller').getRecordTypeId();
    }

	/* Adds a new PO Line */
    public void newLine() {
    	if( null != poLines && 0 < poLines.size() ) {
    		upsert poLines;
    	}
    	if( null == customer || null == reseller ) {
    		ApexPages.addMessage( new ApexPages.message( ApexPages.severity.ERROR, 'Please enter or find both a Reseller and Ship-To Customer first.' ) );
    	} else {
    		if( null == thePO && null != reseller.Id && null != customer.Id ) {
    			thePO = new Purchase_Order__c();
    			thePO.Reseller__c = reseller.Id;
    			thePO.Ship_to_Customer__c = customer.Id;
    			insert thePO;
    		}
    		if( null == poLines ) {
    			poLines = new List<Equipment__c>();
    		}
    		poLines.add( new Equipment__c( Account__c = customer.Id, Related_PO__c = thePO.Id ) );
    	}
    }
    
    // This should save the reseller and refresh the page
    public PageReference saveReseller() {
    	System.debug( 'Got here: ' + reseller.Name );
    	List<Account> lA = [SELECT Id FROM Account WHERE RecordTypeId = :resellerAccountRtId AND Name = :reseller.Name]; 
    	if( 0 == lA.size() ) {
    		foundReseller = 'None Found';
    		System.debug( 'None Found' );
    	} else {
    		reseller = [SELECT Id, Name, Type, Website, ParentId, Phone, Description, Industry, County__c, NumberOfEmployees,
    					BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,
    					ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry
    					FROM Account
    					WHERE RecordTypeId = :resellerAccountRtId AND Name = :reseller.Name LIMIT 1];
    		foundReseller = reseller.Id;
    		System.debug( 'Found: ' + reseller.Id );
    	}
    	return null;
    }
    
    // This should save the customer and refresh the page
    public PageReference saveCustomer() {
    	System.debug( 'Got here: ' + customer.Name );
    	List<Account> lA = [SELECT Id FROM Account WHERE RecordTypeId = :customerAccountRtId AND Name = :customer.Name]; 
    	if( 0 == lA.size() ) {
    		foundCustomer = 'None Found';
    		System.debug( 'None Found' );
    	} else {
    		customer = [SELECT Id, Name, Type, Website, ParentId, Phone, Description, Industry, County__c, NumberOfEmployees,
    					BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,
    					ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry
    					FROM Account
    					WHERE RecordTypeId = :customerAccountRtId AND Name = :customer.Name LIMIT 1];
    		foundCustomer = customer.Id;
    		System.debug( 'Found: ' + customer.Id );
    	}
    	return null;
    }
    
    public PageReference saveAndNew() {
    	
    	try {
    		// Save the Reseller
    		upsert reseller;
    		customer.Reseller__c = reseller.Id;
    		// Save the Customer
    		upsert customer;
    		thePO.Reseller__c = reseller.Id;
    		thePO.Ship_To_Customer__c = customer.Id;
    		// Save the PO
    		upsert thePO;
    		for( Equipment__c e : poLines ) {
    			e.Account__c = customer.Id;
    			e.Related_PO__c = thePO.Id;
    		}
    		if( null != poLines ) {
    			upsert poLines;
    		}
    		// Reset and start over
    		resetEverything();
    	} catch( Exception e ) {
    		System.debug( e );
    		ApexPages.addMessage( new ApexPages.Message( ApexPages.severity.ERROR, 'Exception saving: ' + e.getMessage() ) );
    		return null;
    	}
    	
    	return new PageReference( '/apex/PO_Entry' );
    }
	
	public void resetEverything() {
		thePO = new Purchase_Order__c();
		poLines.clear();
		reseller = new Account();
		customer = new Account();
		foundCustomer = null;
		foundReseller = null;
	} 
    
    public string queryAll(String s){
        String SobjectApiName = s;
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        String query;
        String commaSeparatedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSeparatedFields == null || commaSeparatedFields == ''){
                commaSeparatedFields = fieldName;
            }else{
                commaSeparatedFields = commaSeparatedFields + ', ' + fieldName;
            }
        }
 
        query = commaSeparatedFields + ' from ' + SobjectApiName;
 
        return query;
    }
}