public class CustomerCommunityHeaderLabelController {

     @AuraEnabled
    public static List<String> getResellerName() {
        List<Contact> contactList = new List<Contact>();
        Account acc = new Account();
        String welcomeLabel='';
        List<String> logoAndWelcomeLabel = new List<String>();
        Id p = [select id from profile where name='S3P Customer Community Plus Login User'].id;
        List<User> userObj = [SELECT id,ContactId FROM USER Where Id =: UserInfo.getUserId() and profileId =: p];
        
        if(userObj != null && userObj[0] != null) {
            contactList = [SELECT id,Name,AccountId 
                           FROM Contact 
                           WHERE Id = : userObj[0].ContactId];
            
            if(contactList != null && contactList.size() > 0 && contactList[0] != null) {
                acc = [SELECT Reseller__r.Name,Reseller__r.Image_URL__c  from Account where Id = :contactList[0].AccountId];
                welcomeLabel = Label.CustomerCommunityHeaderLabel+' '+acc.Reseller__r.Name+'!';
                System.debug('acc----------'+acc);
                logoAndWelcomeLabel.add(welcomeLabel);
                logoAndWelcomeLabel.add(acc.Reseller__r.Image_URL__c);
            }
            /*acc = [SELECT Reseller__r.Name from Account where Id = '0010q00000OJmP9'];
            welcomeLabel = Label.CustomerCommunityHeaderLabel+' '+acc.Reseller__r.Name;
            System.debug('acc----------'+acc);*/
            
        }
        return logoAndWelcomeLabel;
    }
    
}