({
   save : function(component,event,helper) {
       helper.handleSave(component, event, helper);
     },
    Submit:function(component,event,helper){
        debugger;
        helper.onSubmit(component, event);
        var bu = component.get("v.uploadCase");
        component.set("v.uploadCase", true);
     
    },
    closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.uploadCase", false);
          component.set("v.ErrorMessageBlock", false);
        component.set("v.DuplicateMessageBlock", false);
        component.set("v.SuccessMessageBlock", false);
   },
    handleClick: function (component, event, helper) {
         var csvStringResult, counter, keys, columnDivider, lineDivider;
        columnDivider = ',';
        lineDivider =  '\n';
 
        // in the keys valirable store fields API Names as a key 
        // this labels use in CSV file header  
        keys = ['FirstName','LastName','Department','MobilePhone','Id' ];
        
        csvStringResult = '';
        csvStringResult += keys.join(columnDivider);
        csvStringResult += lineDivider;
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvStringResult);
        hiddenElement.target = '_self'; // 
        hiddenElement.download = 'ExportData.csv';  // CSV file Name* you can change it.[only name not .csv] 
        document.body.appendChild(hiddenElement); // Required for FireFox browser
        hiddenElement.click();
    },
    loadContactList: function(component, event, helper){
       helper.onLoad(component, event);
    }
    
})