({
   handleSave : function(component, event, helper) {
        debugger;
        var maxStringSize = 6000000; //Maximum String size is 6,000,000 characters
        var maxFileSize = 19242880; //After Base64 Encoding, this is the max file size
        var chunkSize = 950000; //Maximum Javascript Remoting message size is 1,000,000 characters
        var attachment;
        var attachmentName;
        var fileSize;
        var positionIndex;
        var fileInput = component.find("file").getElement();
        var file = fileInput.files[0];
        
        var messageToDisplay = 'Record Save successfully found for Serial number';
        var recordId = component.get("v.recordId"); 
        //helper.toggleSpinner(component);
        
        if (file !== undefined && file !== '' && file !== null) {
            helper.toggleSpinner(component);
            var fileNameTest = fileInput.value;
            var ext = fileNameTest.substring(fileNameTest.lastIndexOf('.'));
            if(ext == ".csv"){
                 if (file.size <= maxFileSize) {   
                    var  attachmentName =file.name;
                    console.log('attachmentName>>>>>'+attachmentName);
                    var fileReader = new FileReader();
                    fileReader.onloadend = function(e) {
                        console.log('I am here.');
                        var stringvalue=fileReader.result;
                        attachment = stringvalue; //Base 64 encode the file before sending it
                        positionIndex = 0;
                        fileSize =attachment.length;
                        var self = this;
                        var action=component.get("c.readFile");
                        action.setParams({attachment : attachment});
                        action.setCallback(this, function(response) {
                            debugger;
                            var result = response.getReturnValue();
                            
                            var responseVal = '';
                            var state = response.getState();
                            console.log(state);
                             var ErrorList =[];
                            var DuplicateList =[];
                            var SuccessList =[];
                            if(result.length>0){ //&& result.length>0
                                for(var i=0; i<result.length;i++){
                                    var msg=result[i].message;
                                    
                                    if(msg==='Error'){
                                         var dateFormat = {};
                                        dateFormat = "please provide date in format(mm/dd/yyyy) / Start Date is greater than End Date found for Serial number "+result[i].serialNumber;
                                        ErrorList.push(dateFormat);
                                        
                                        
                                        component.set("v.ErrorMsg", ErrorList);
                                        component.set("v.ErrorMessageBlock", true);
                                       /* var toastEvent = $A.get("e.force:showToast");
                                            toastEvent.setParams({
                                                "type": "Error", 
                                                "title": "Error",
                                                "message": "please provide date in format(mm/dd/yyyy) found for Serial number "+result[i].serialNumber
                                            });
                                            toastEvent.fire();
                                            component.set("v.uploadCase", false); */
                                    }else if(msg==='Duplicate'){
                                         var DuplicateRecord = {};
                                        DuplicateRecord = "Duplicate record found for Serial number "+result[i].serialNumber;
                                        DuplicateList.push(DuplicateRecord);
                                        
                                        
                                        component.set("v.DuplicateMsg", DuplicateList);
                                        component.set("v.DuplicateMessageBlock", true);
                                        /* var toastEvent = $A.get("e.force:showToast");
                                            toastEvent.setParams({
                                                "type": "Warning", 
                                                "title": "Warning",
                                                "message": "Duplicate record found for Serial number "+result[i].serialNumber
                                            });
                                            toastEvent.fire();
                                            component.set("v.uploadCase", false); */
                                    }else if(msg==='Success'){
                                         var SuccessRecord = {};
                                        SuccessRecord = "Asset uploaded for serial number "+result[i].serialNumber;
                                        SuccessList.push(SuccessRecord);
                                        
                                        
                                        component.set("v.SuccessMsg", SuccessList);
                                        component.set("v.SuccessMessageBlock", true);
                                        /* var toastEvent = $A.get("e.force:showToast");
                                            toastEvent.setParams({
                                                "type": "Success", 
                                                "title": "Success",
                                                "message": messageToDisplay +result[i].serialNumber
                                            });
                                            toastEvent.fire();
                                            component.set("v.uploadCase", false); */
                                    }else if(msg==='CustomMetaDataError'){
                                        var dateFormat = {};
                                        dateFormat = "please see custom metadata(CSV_Column__mdt) field records";
                                        ErrorList.push(dateFormat);
                                        
                                        
                                        component.set("v.ErrorMsg", ErrorList);
                                        component.set("v.ErrorMessageBlock", true);
                                        
                                    }
                                }
                            }
                        });
                        $A.enqueueAction(action);
                        
                        component.set("v.result", false);
                        
                        component.find("file").getElement().value='';//to de-select the file
                        
                    }
                    fileReader.readAsBinaryString(file);
                    
                }
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "Fail", 
                    "title": "Fail",
                    "message": 'Upload CSV File'
                });
                toastEvent.fire();
                component.set("v.uploadCase", false);
            }
        }
    },
    toggleSpinner : function(component){
        window.setTimeout(
            $A.getCallback(function() {
                var spinner = component.find("UploadFile");
                if(spinner !== null && spinner !== '' && spinner !== undefined){
                    $A.util.removeClass(spinner, "slds-hide");
                    $A.util.addClass(spinner, "slds-show");
                }
            }), 200
        )      
    },
    onSubmit: function(component, event) {
      //call apex class method
      debugger;
        var action = component.get('c.fetchDocument');
      action.setCallback(this, function(response){
         //store state of response
         var state = response.getState();
         if (state === "SUCCESS") {
               debugger;
             var documentObj = response.getReturnValue();
             var fileId =documentObj.Id;
             var locationUrl = location.origin;
             var dLink=locationUrl+'/servlet/servlet.FileDownload?file='+fileId;
             var CommunityUrl = $A.get("{!$Label.c.PartialCommunityUrl}");
            // https://smart3rdparty--partial--c.documentforce.com/servlet/servlet.FileDownload?file=0150m0000001Zx4
             component.set('v.downloadLink',CommunityUrl+fileId);
            //set response value in ListOfContact attribute on component.
            //component.set('v.ListOfContact', response.getReturnValue());
         }
      });
      $A.enqueueAction(action);
    },
    onLoad: function(component, event) {
      //call apex class method
      debugger;
        var action = component.get('c.getResellerName');
      action.setParams({});  
        action.setCallback(this, function(a) {
            var result = a.getReturnValue();
            component.set("v.customerWelcomeLabel", result);
        });
        $A.enqueueAction(action); 
    }
})