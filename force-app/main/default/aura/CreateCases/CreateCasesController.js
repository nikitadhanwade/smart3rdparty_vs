({
    doInit: function(component, event, helper) {
        debugger;
         // this function call on the component load first time     
      // get the page Number if it's not define, take 1 as default
      component.set("v.hadselected", true);
      var page = component.get("v.page") || 1;
      // get the select option (drop-down) values.   
      var recordToDisply = '5';
      // call the helper function   
      helper.getEquipmentList(component, page, recordToDisply);
 
    },
    searchKeyChange1: function(component, event, helper) {
        debugger;
        component.set("v.hadselected", true);
        var searchKey = event.getParam("searchKey");
        
            var action = component.get("c.findSerialNumber");
        
        action.setParams({
            "searchKey": searchKey
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            var serialNumberlist =  a.getReturnValue();
            component.set("v.venueRecordLimitList", serialNumberlist);
            //helper.onSelect(component, event);
        });
        $A.enqueueAction(action);
        
 

    },
	handleClick : function(component, event, helper) {
        debugger;
        var searchInput = component.find("searchInput");
    	var searchText = searchInput.get("v.value");
      	var action = component.get('c.searchForIds');
      action.setParams({searchText: searchText});
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === 'SUCCESS') {
          var SerialNumber = response.getReturnValue();
            sessionStorage.setItem('customSearch--recordIds', JSON.stringify(SerialNumber));
          var navEvt = $A.get('e.force:navigateToURL');
          navEvt.setParams({url: '/custom-search-results'});
          navEvt.fire();
          console.log(SerialNumber);
        }
      });
      $A.enqueueAction(action);
    },
    searchKeyChange: function(component, event, helper) {
        debugger;
        var myEvent = $A.get("e.c:SearchEvent");
        myEvent.setParams({"searchKey": event.target.value});
        myEvent.fire();
    },

    create : function(component, event, helper) {
        debugger;
		console.log('Create record');
        
        //getting the candidate information
        var manufacturer = component.find("manufacturer").get("v.value");
        var SerialNumber = component.find("SerialNumber").get("v.value");
        var Subject = component.find("Subject").get("v.value");
        var Description = component.find("Description").get("v.value");
        //Validation
        /*if($A.util.isEmpty(candidate.First_Name__c) || $A.util.isUndefined(candidate.First_Name__c)){
            alert('First Name is Required');
            return;
        }            
        if($A.util.isEmpty(candidate.Last_Name__c) || $A.util.isUndefined(candidate.Last_Name__c)){
            alert('Last Name is Rqquired');
            return;
        }
        if($A.util.isEmpty(candidate.Email__c) || $A.util.isUndefined(candidate.Email__c)){
            alert('Email is Required');
            return;
        }*/
        /*if($A.util.isEmpty(caseData.SSN__c) || $A.util.isUndefined(caseData.SSN__c)){
            alert('SSN is Required');
            return;
        }*/
        if(Subject == '' || Subject == null){
            //Subject.set("v.errors", [{message:"Input not a number: "}]);
            component.find("Subject").set("v.errors", [{message:"Please enter Subject"}]);
            //alert('Subject is Required');
            return;
        } 
        //Calling the Apex Function
        var action = component.get("c.createRecord");
        
        //Setting the Apex Parameter
        action.setParams({
            "manufacturer":manufacturer,
                "SerialNumber": SerialNumber,
            "Subject": Subject,
            "Description": Description
        });
        
        //Setting the Callback
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var newCaseId = a.getReturnValue()
                                 var newCaseId = a.getReturnValue();
                var urlEvent = $A.get("e.force:navigateToURL");
                 var urls=window.location.href;
                urlEvent.setParams({
                   
                   "url": urls+"case/"+newCaseId+"/detail" 
                    //"url": "/500/"+newCaseId+"/"
                });
                urlEvent.fire();
                //component.set("v.isOpen", false);
                /*component.set("v.CreateCase", false);
            var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        "title": "Success!",
        "message": "The record has been inserted successfully."
    });
                //alert('success');
    toastEvent.fire();*/
                //alert('Record Save Successfully');
            } else if(state == "ERROR"){
                alert('Error in calling server side action');
            }
        });
        
		//adds the server-side action to the queue        
        $A.enqueueAction(action);

	},
    selectonRadio: function(component, event, helper) {
        debugger;
        //var temp= [];
        //component.set("v.selectedVenues",temp);
        /*$('table tr').click(function() {
            $(this).find('input:radio').prop('checked', true);
            var venueId = $(this).find('input:radio').attr('value');
            var selectedEquipment = component.get("v.selectedEquipment");
            if (venueId != null || venueId != undefined) {

                selectedEquipment.push(venueId);

            }
            component.set("v.juncselectedVenuesobj", selectedEquipment);
        });*/
        //var selected = event.getSource().get("v.text");
        //alert(selected);
         var checkboxesChecked = component.get("v.selectedEquipment");
        
        if(event.target.value == 'on'){
            component.set("v.selectedEquipment", '');
        }else{
            component.set("v.selectedEquipment", event.target.value);
        }
        
         var checkboxesCheckedFlag = component.get("v.selectedEquipmentFlag");
        component.set("v.selectedEquipmentFlag", event.currentTarget.checked);
        
        var selectedItem = event.currentTarget; 
        component.set("v.Manufacture", selectedItem.dataset.record);
        helper.handleChange(component, event);
        component.set('v.TabId', "venueid");


    },
    Submit: function(component, event, helper) {
        debugger;
        //var checkboxesChecked = component.get("v.selectedEquipment");
        //var selectedsubvenues = component.get("v.selectedsubvenue");
        //window.scrollBy(0, 1000);
		var SrNo = document.getElementById("SrNo").value;
        var checkboxesChecked = component.get("v.selectedEquipment");
        var checkboxesCheckedFlag = component.get("v.selectedEquipmentFlag");
        var Manufacture = component.get("v.Manufacture");
        //if ((SrNo == null || SrNo == '') && (checkboxesCheckedFlag == '' || checkboxesCheckedFlag == null)) {
        
        var action = component.get("c.getPicklistValue");
        
        action.setParams({
            "Manufacture": Manufacture
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            var getPickListData =  a.getReturnValue();
                var industryMap = [];
                for(var key in getPickListData){
                    industryMap.push({key: key, value: getPickListData[key]});
                }
                component.set("v.industryMap", industryMap);
        });
        $A.enqueueAction(action);
        
       if ((checkboxesCheckedFlag == '' || checkboxesCheckedFlag == null)) {   
        component.set("v.isToastError", true);
            component.set("v.displayedSection","section2");
            //component.find("SrNo").set("v.errors", [{message:"Please enter Serial Number"}]);
        } else {
            var venuelist = component.get("v.selectedEquipment");
            if(venuelist=='on')
            {
                component.set("v.selectedEquipment", '');
            }
            component.set("v.isToastError", false);
            component.set("v.displayedSection","section1");
        } 
       /*var scrollOptions = {
            left: 0,
            top: 0,
            behavior: 'smooth'
        }
        window.scrollTo(scrollOptions);*/
        
    },
     closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.CreateCase", false);
          
   },
    onClickfirstPage: function(component, event, helper) {
        component.set("v.currentPageNumber", 1);
        component.set("v.pageNumber", 1);
        if (component.get("v.pageNumber") === 1) {
            component.set("v.firstclick", true);
            component.set("v.prevclick", true);
            component.set("v.nextclick", false);
            component.set("v.lastclick", false);
            helper.onClickRenderPage(component);
        }
    },
    onClickprevPage: function(component, event, helper) {
        var pageN = component.get("v.pageNumber");
        pageN--;
        component.set("v.currentPageNumber", Math.max(component.get("v.currentPageNumber") - 1, 1));
        component.set("v.pageNumber", pageN);
        if (component.get("v.pageNumber") === 1) {
            component.set("v.firstclick", true);
            component.set("v.prevclick", true);
            component.set("v.nextclick", false);
            component.set("v.lastclick", false);
            helper.onClickRenderPage(component);
        } else if (component.get("v.pageNumber") < component.get("v.maxPageNumber")) {
            component.set("v.firstclick", false);
            component.set("v.prevclick", false);
            component.set("v.nextclick", false);
            component.set("v.lastclick", false);
            helper.onClickRenderPage(component);
        }
    },
    onClicknextPage: function(component, event, helper) {
        debugger;
        var pageN = component.get("v.pageNumber");
        pageN++;
        component.set("v.currentPageNumber", Math.min(component.get("v.currentPageNumber") + 1, component.get("v.maxPageNumber")));
        component.set("v.pageNumber", pageN);
        if (component.get("v.pageNumber") < component.get("v.maxPageNumber")) {
            component.set("v.firstclick", false);
            component.set("v.prevclick", false);
            component.set("v.nextclick", false);
            component.set("v.lastclick", false);
            helper.onClickRenderPage(component);
        } else if (component.get("v.pageNumber") == component.get("v.maxPageNumber")) {
            component.set("v.firstclick", false);
            component.set("v.prevclick", false);
            component.set("v.nextclick", true);
            component.set("v.lastclick", true);
            helper.onClickRenderPage(component);
        }

    },
    onClicklastPage: function(component, event, helper) {
        component.set("v.currentPageNumber", component.get("v.maxPageNumber"));
        component.set("v.pageNumber", component.get("v.maxPageNumber"));
        if (component.get("v.pageNumber") == component.get("v.maxPageNumber")) {
            component.set("v.firstclick", false);
            component.set("v.prevclick", false);
            component.set("v.nextclick", true);
            component.set("v.lastclick", true);
            helper.onClickRenderPage(component);
        }
    },
    
    
    
    
   
   navigate: function(component, event, helper) {
      // this function call on click on the previous page button  
      var page = component.get("v.page") || 1;
      // get the previous button label  
      var direction = event.getSource().get("v.label");
      // get the select option (drop-down) values.  
      var recordToDisply = component.find("recordSize").get("v.value");
      // set the current page,(using ternary operator.)  
      page = direction === "Previous Page" ? (page - 1) : (page + 1);
      // call the helper function
      helper.getEquipmentList(component, page, recordToDisply);
 
   },
 
   onSelectChange: function(component, event, helper) {
      // this function call on the select opetion change,	 
      var page = 1
      var recordToDisply = component.find("recordSize").get("v.value");
      helper.getEquipmentList(component, page, recordToDisply);
   },
    sortManufacturer: function(component, event, helper) {
        debugger;
       // set current selected header field on selectedTabsoft attribute.     
       component.set("v.selectedTabsoft", 'Manufacturer__c');
       // call the helper function with pass sortField Name   
       helper.sortHelper(component, event, 'Manufacturer__c');
    },
    sortModelName: function(component, event, helper) {
        debugger;
       // set current selected header field on selectedTabsoft attribute.     
       component.set("v.selectedTabsoft", 'Model_Name__c');
       // call the helper function with pass sortField Name   
       helper.sortHelper(component, event, 'Model_Name__c');
    },
    sortModelNumber: function(component, event, helper) {
        debugger;
       // set current selected header field on selectedTabsoft attribute.     
       component.set("v.selectedTabsoft", 'Model_Number__c');
       // call the helper function with pass sortField Name   
       helper.sortHelper(component, event, 'Model_Number__c');
    },
    sortSerialNumber: function(component, event, helper) {
        debugger;
       // set current selected header field on selectedTabsoft attribute.     
       component.set("v.selectedTabsoft", 'Serial_Number__c');
       // call the helper function with pass sortField Name   
       helper.sortHelper(component, event, 'Serial_Number__c');
    },
    handleCompanyOnChange : function(component, event, helper) {
        debugger;
        var indutry = component.get("v.Manufacture");
        component.set("v.Manufacture", indutry);
        
    }
   
})