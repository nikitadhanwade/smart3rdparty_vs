({
	submitForm: function(component) {
		var firstName = component.find('firstName').get('v.value'),
			lastName = component.find('lastName').get('v.value'),
			toastEvent = $A.get("e.force:showToast");
	    toastEvent.setParams({
    		title: 'Welcome ',
    		message: firstName + ' ' + lastName + '!'
	    });
	    toastEvent.fire();
	},
    onClickRenderPage: function(component) {
        debugger;
    var records = component.get("v.serialNumberlist"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber-1)*10, pageNumber*10);
          component.set("v.venueRecordLimitList", pageRecords);
  },
    onSelect:function (component){
        
        component.set("v.pageNumber",1);
        debugger;
              var resultEmp = component.get("v.serialNumberlist");
                component.set("v.totalRecordsCount",resultEmp.length);
                component.set("v.oppRecordList",resultEmp);
                component.set("v.maxPage", Math.floor((resultEmp.length+9)/10));
                component.set("v.maxPageNumber", Math.floor((resultEmp.length+9)/10));
                debugger;
              if(resultEmp === null || resultEmp === undefined || resultEmp.length ===  0){
                component.set("v.pageNumber",0); 
                component.set("v.firstclick",true);
                component.set("v.prevclick",true);
                component.set("v.nextclick",true);
                component.set("v.lastclick",true);  
                }
                this.onClickRenderPage(component);    //calling helper's own method 
                 component.set("v.firstclick",true);
                 component.set("v.prevclick",true);
                 component.set("v.nextclick",false);
                 component.set("v.lastclick",false);       
              if(component.get("v.pageNumber") == component.get("v.maxPageNumber")){ 
                 component.set("v.firstclick",true);
                 component.set("v.prevclick",true);
                 component.set("v.nextclick",true);
                 component.set("v.lastclick",true); 
                }    
      },
    handleChange : function(component,event,helper){
        debugger;
        
        //component.set("v.selectedVenues",selectedsubvenues);
        
        //event.currentTarget.checked
        
        //event.currentTarget.dataset.record
        
        var venuelist = component.get("v.selectedEquipment");
        component.set("v.selectedEquipment",venuelist);
        
       	var a = event.currentTarget.checked;
        
        var b = event.currentTarget.dataset.record;
        
        var selectedsubvenues = component.get("v.selectedEquipment");
        //var temp= [];
       	//component.set("v.selectedVenues",temp);
        
       /* if(b != null || b != undefined){
            if(a === true){
                selectedsubvenues.push(b);
            } 
        }
        if(b != null || b != undefined){
            if(a === false){
                selectedsubvenues.splice(selectedsubvenues.indexOf(b),  1);
            } 
        }*/
      
        component.set("v.selectedEquipment",selectedsubvenues);
    },
    
    

    getEquipmentList: function(component, page, recordToDisply) {
        debugger;
        
     
      // create a server side action. 
      var action = component.get("c.getEquipmentData");
      // set the parameters to method 
      action.setParams({
         "pageNumber": page,
         "recordToDisply": recordToDisply
      });
      // set a call back   
      action.setCallback(this, function(a) {
         // store the response return value (wrapper class insatance)  
         var result = a.getReturnValue();
          component.set("v.acctList", result);
         console.log('result ---->' + JSON.stringify(result));
         // set the component attributes value with wrapper class properties.   
 
         //component.set("v.Accounts", result.eqipList);
         component.set("v.pageSize", result.pageSize);
         component.set("v.offset", result.offset);
         component.set("v.page", result.page);
         component.set("v.total", result.total);
         component.set("v.pages", Math.ceil(result.total / recordToDisply));

            component.set("v.venueRecordLimitList",result.eqipList);
            
       
      });
      // enqueue the action 
      $A.enqueueAction(action);
   },
    sortHelper: function(component, event, sortFieldName) {
        debugger;
      var currentDir = component.get("v.arrowDirection");
 var pageSize = component.get("v.pageSize");
        var offset = component.get("v.offset");
        
      if (currentDir == 'arrowdown') {
         // set the arrowDirection attribute for conditionally rendred arrow sign  
         component.set("v.arrowDirection", 'arrowup');
         // set the isAsc flag to true for sort in Assending order.  
         component.set("v.isAsc", true);
      } else {
         component.set("v.arrowDirection", 'arrowdown');
         component.set("v.isAsc", false);
      }
      // call the onLoad function for call server side method with pass sortFieldName 
      var action = component.get('c.fetchContact');
 
      // pass the apex method parameters to action 
      action.setParams({
         'sortField': sortFieldName,
         'isAsc': component.get("v.isAsc"),
          'PS' :pageSize,
          'OS' :offset
          
      });
      action.setCallback(this, function(response) {
         //store state of response
         var state = response.getState();
          var r = response.getReturnValue();
         if (state === "SUCCESS") {
            //set response value in ListOfContact attribute on component.
            component.set('v.venueRecordLimitList', response.getReturnValue());
         }
      });
      $A.enqueueAction(action);
   }
})