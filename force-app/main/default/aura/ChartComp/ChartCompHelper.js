({
	createGraph : function(cmp, temp) {
        
        var dataMap = {"chartLabels": Object.keys(temp),
                       "chartData": Object.values(temp)
                       };
        
        var el = cmp.find('barChart').getElement();
        var ctx = el.getContext('2d');
        
        new Chart(ctx, {
            type: 'bar',
            height : 800,
            responsive : true,
            data: {
                labels: dataMap.chartLabels,
                datasets: [
                    {
                        label: '# of Tickets',
                        backgroundColor: "rgba(0, 163, 204,8)",
                        data: dataMap.chartData
                    }
                ],
                borderWidth: 10
            },
             maintainAspectRatio: false,
    options: {
        responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                    
                }
            }],
             xAxes: [{
                ticks: {
					
            			fontSize:8
             	}
            }],
            pointLabels: {
      			pointLabelFontSize: 6
    		}
        }
    }
        });
	}
    /*createLineGraph : function(cmp, temp) {
        
        var label = [];
        var firstValue = [];
        var secondValue = [];
        
        for(var a=0; a< temp.length; a++){
            console.debug(temp[a]["label"]);
            label.push(temp[a]["label"]);
            firstValue.push(temp[a]["firstValue"]);
            secondValue.push(temp[a]["secondValue"]);                     
        }    
        var el = cmp.find('lineChart').getElement();
        var ctx = el.getContext('2d');
        
        new Chart(ctx, {
            type: 'line',
            data: {
                    labels: label,
                    datasets: [{
                      label: 'First Values',
                      data: firstValue,
                      backgroundColor: "rgba(51,51,255,0.4)"
                    }, {
                      label: 'Second Values',
                      data: secondValue,
                      backgroundColor: "rgba(255,153,0,0.4)"
                    }]
                  }
        });
        
	}*/
    
})