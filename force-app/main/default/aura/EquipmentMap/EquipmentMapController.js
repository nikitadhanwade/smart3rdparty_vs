({
    jsLoaded: function(component, event, helper) {
        
        var map = null;
        map = L.map('map', {zoomControl: true, tap: false}).setView([34.058964 ,-108.170849],3);
        L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
            {
                attribution: ''
            }).addTo(map);
        component.set("v.map", map);
    },
    equipmentsLoaded: function(component, event, helper) {
        // Add markers
        var map1 = component.get('v.map');
        var equipments = event.getParam('equipments');
        for (var i=0; i<equipments.length; i++) {
            var equipment = equipments[i];
            var latLng = [equipment.Site__r.Location__Latitude__s,equipment.Site__r.Location__Longitude__s];
            console.log(latLng);
            debugger;
            L.marker(latLng, {equipment: equipment}).addTo(map1);
        }  
    }
})