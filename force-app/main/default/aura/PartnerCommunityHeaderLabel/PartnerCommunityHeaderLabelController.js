({
    doInit: function(component, event, helper) {
        debugger;
        
        var action = component.get("c.getAccountName");
        action.setParams({});  
        action.setCallback(this, function(a) {
            var result = a.getReturnValue();
            component.set("v.partnerWelcomeLabel", result[0]);
            component.set("v.LogoURL", result[1]);
        });
        $A.enqueueAction(action); 
    }
})