({
    onLoad: function(component, event) {
        debugger;
        console.log('onLoad call');
        //call apex class method
        var action = component.get('c.fetchAsset');
        action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                //set response value in ListOfContact attribute on component.
                component.set('v.ListOfAsset', response.getReturnValue());
                // set deafult count and select all checkbox value to false on load 
                component.set("v.selectedCount", 0);
                //component.find("box3").set("v.value", false);
                component.set("v.buttonClick", true);
            }
        });
        $A.enqueueAction(action);
    },
    non3rdPartySendMail: function(component, event, deleteRecordsIds) {
        debugger;
        //call apex class method
        var action = component.get('c.non3rdPartySendMail');
        var fromDate = component.get("v.fromDate");
        var toDate = component.get("v.toDate");
        var requestDescription = component.find("requestDescription").get("v.value");
        // pass the all selected record's Id's to apex method 
        action.setParams({
            "lstRecordId": deleteRecordsIds,
            "fromDate":fromDate,
            "toDate":toDate,
            "requestDescription":requestDescription
            
            
        });
        action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                                            toastEvent.setParams({
                                                "type": "Success", 
                                                "title": "Success",
                                                "message": "Request sent to smart3rdParty successfully"
                                            });
                                            toastEvent.fire(); 
                component.set("v.buttonClick", false);
                //this.onLoad(component, event);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                                            toastEvent.setParams({
                                                "type": "Error", 
                                                "title": "Error",
                                                "message": "Mail is not sent successfully"
                                            });
                                            toastEvent.fire(); 
            }
        });
        $A.enqueueAction(action);
    },
})